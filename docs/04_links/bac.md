title: bac NSI

# Documents pour le Bac NSI

## Les épreuves pratiques 

- [Education.gouv](https://www.education.gouv.fr/bo/2024/Hebdo8/MENE2330918N){target="_blank"} : Bulletin Officiel

- [Sujets d'EP](https://cyclades.education.gouv.fr/delos/public/listPublicECE){target="_blank"} : accès officiel à la banque des sujets d'EP

- [Epreuves pratiques](https://ecebac.fr/listaca.php?mat=94){target="_blank"} sur le site CCBac.fr :  épreuves pratiques du Baccalauréat, avec corrections.




- [Epreuves pratiques](https://glassus.github.io/terminale_nsi/T6_6_Epreuve_pratique/BNS_2024/){target="_blank"} par Gilles Lassus : les sujets d'EP avec correction 

- [Epreuves pratiques](https://fabricenativel.github.io/index_annales/){target="_blank"} par Fabrice Nativel : les sujets d'EP corrigés 

- [Epreuves pratiques](https://progalgo.fr/page7.php){target="_blank"} Les sujets d'EP sur le site Proalgo

- [codepuzzle banque de défis](https://www.codepuzzle.io/defis-banque){target="_blank"} Liste des sujets de l'épreuve pratique à intégrer dans une classe pour proposer des entraînements aux élèves et suivre leur progression

-------------------------------------------------------------------

## Sujets officiels Bac écrit 

- [education.gouv 2024](https://www.education.gouv.fr/reussir-au-lycee/bac-2024-les-sujets-des-epreuves-ecrites-de-specialite-414414){target="_blank"}

- [education.gouv 2023](https://www.education.gouv.fr/reussir-au-lycee/bac-2023-les-sujets-des-epreuves-ecrites-de-specialite-357743){target="_blank"}  

-  [education.gouv 2022](https://www.education.gouv.fr/reussir-au-lycee/bac-2022-les-sujets-des-epreuves-ecrites-de-specialite-341303){target="_blank"} 


- [Annales du Bac NSI](https://mmonnier.forge.apps.education.fr/tnsi/Annales/24-NSIJ1AN1/){target="_blank"} par Marius Monnier : sujets sur le site avec corrections 

- [Annales du Bac NSI](https://glassus.github.io/terminale_nsi/T6_Annales/liste_sujets/){target="_blank"} par Gilles Lassus : pdf des sujets

- [Annales du Bac NSI](https://fabricenativel.github.io/index_annales/){target="_blank"} par Fabrice Nativel : sujets, description du contenu et propositions de corrections 

- [Annales du Bac NSI](http://projet.eu.org/pedago/sin/NSI/sujets/){target="_blank"} Sciences Informatique et Numérique : Ressources pédagogiques Libres et gratuites, sujets et corrections.

----------------------------------------

## Rattrapage

- [Rattrapage du Bac](https://nsi-snt.ac-normandie.fr/exercices-pour-l-oral-de-controle-en-nsi){target="_blank"} Académie de Normandie : exemples d'exercices pour le rattrapage

----------------------------------------

## Epreuve fin de première

[Sujets de fin de première NSI](https://ccbac.fr/list.php?session=2&voie=1&mat=26&opt=0&epr=0&an=2024){target="_blank"} sur le site CCBac.fr par l'association UPECS : référence tous les sujets de la BNS pour le contrôle continu.


