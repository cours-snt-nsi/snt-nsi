title: modules python

# Modules Pythons

- Module **Flask** : un micro-framework  permettant de concevoir une application web [Site officiel](https://flask.palletsprojects.com/en/3.0.x/){target="_blank"}

Tutoriels vidéo Flask : [Xavki](https://www.youtube.com/watch?v=vunWCJrwKx8){target="_blank"} et [Paul Vincensini](https://www.youtube.com/watch?v=zDkUIKedFsU){target="_blank"}

[Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-make-a-web-application-using-flask-in-python-3-fr){target="_blank"} Comment créer une application web en utilisant Flask en Python 3


[Spectre](https://picturepan2.github.io/spectre/){target="_blank"} A Lightweight, Responsive and Modern CSS Framework

[BootStrap](https://getbootstrap.com/){target="_blank"} Build fast, responsive sites with Bootstrap


- - - - - -

- Module **Pillow** : Travailler sur des images en Python [Documentation](https://pillow.readthedocs.io/en/stable/){target="_blank"} ; Tutoriel [Zet Code](https://zetcode.com/python/pillow/){target="_blank"} ;  cours  [Lycee Blaise Pascal](http://info.blaisepascal.fr/pillow){target="_blank"}




- - - - - -

- Module **mysql.connector** et **mariadb** :  utiliser Python pour se connecter à une base de données avec MySQL

[Exemples avec mysql.connector](https://dev.mysql.com/doc/connector-python/en/connector-python-examples.html){target="_blank"} 

[Présentation des bases avec mariadb](https://mariadb.com/fr/resources/blog/how-to-connect-python-programs-to-mariadb/){target="_blank"}

- - - - - -

- Module **Mocodo** :  un logiciel d'aide à l'enseignement et à l'apprentissage des bases de données relationnelles

[Document d'accompagnement](https://rawgit.com/laowantong/mocodo/master/doc/fr_refman.html){target="_blank"} 

- - - - - -

- Module **Turtle** :  Le module Turtle est un ensemble d'outils permettant de dessiner à l'aide d'instructions simples des figures géométriques.

[Tutoriel](https://turtle-tutorial.readthedocs.io/en/latest/2_draw/draw.html){target="_blank"} 

[Trinket](https://hourofpython.trinket.io/a-visual-introduction-to-python#/welcome/an-hour-of-code){target="_blank"} Introduction visuel au module Turtle

- - - - - -

- Module **Pyxel** :  Un moteur de jeu vidéo rétro pour Python.

[tutoriel Pyxel (nuit du code)](https://nuit-du-code.forge.apps.education.fr/DOCUMENTATION/PYTHON/01-presentation/){target="_blank"}  et [Repository Pyxel](https://github.com/kitao/pyxel/blob/main/docs/README.fr.md){target="_blank"}

[Pyxel Studio](https://www.pyxelstudio.net/){target="_blank"} Online Development Environment for PYXEL




- - - - - -

- Module **tkinter** : 

[customtkinter](https://customtkinter.tomschimansky.com/){target="_blank"} une version modernisée de tkinter

[module tkutils](https://pypi.org/project/tkutils/){target="_blank"} de Frédéric Zinelli : some additional logic and syntactic sugar when using tkinter. The main goal is to fill some gaps of tkinter, which might make it very annoying to use.


- - - - - -

- Module **PyQT** : 

[PyQT](https://riverbankcomputing.com/software/pyqt/){target="_blank"} pour développer des interfaces graphiques complexes pour des outils de visualisation de données, des logiciels multimédias interactifs, des applications de bureau et des applications commerciales ou scientifiques complexes

[vidéo](https://peertube.lyceeconnecte.fr/w/x93rbAt6SPU3ibYea2mevw){target="_blank"} tutoriel par Pierre Marquestaut


- - - - - -

- Module **dearpygui** :  an easy-to-use, dynamic, GPU-Accelerated, cross-platform graphical user interface toolkit(GUI) for Python



[Documentation](https://dearpygui.readthedocs.io/en/latest/index.html){target="_blank"} et [Documentation Pypi](https://pypi.org/project/dearpygui/){target="_blank"} 

[Tutoriel Vidéo](https://www.youtube.com/playlist?list=PL-Q7fIakgvUBR2PNPoGqb9o-koAzFwoDs){target="_blank"}  de Fred Leleu

- - - - - -

- Module **Curses** :  interface graphique pour contrôler l'affichage en mode texte



[Documentation complète](https://docs.python.org/fr/3/library/curses.html){target="_blank"} et [Documentation](https://docs.python.org/fr/3/howto/curses.html){target="_blank"}


[Tutoriel vidéo](https://www.youtube.com/watch?v=Db4oc8qc9RU){target="_blank"} de la chaine Tech with Tim

[Tutoriel Dev_dungeon](https://www.devdungeon.com/content/curses-programming-python){target="_blank"} et [tutoriel RuneBook.dev](https://runebook.dev/fr/docs/python/library/curses){target="_blank"}

- - - - - -

- **Pygame Zero** : un wrapper autour de pygame pour en simplifier l’utilisation

[Documentation de  Daniel Pope](https://pgzero-french.readthedocs.io/fr/latest/){target="_blank"}

[Tutoriel de Yves Moncheaux](https://clogique.fr/nsi/premiere/tuto_pg0/tuto_pgzero.html#1){target="_blank"}

- - - - - -

- **Ursina** : Interface graphique en 3D en Python


[Documentation (Anglais)](https://www.ursinaengine.org/){target="_blank"}

[TP Ursacraft](https://clogique.fr/nsi/premiere/td_tp/TP_Ursacraft.html#1){target="_blank"}

[Tutoriel PDF](https://www.ursinaengine.org/ursina_for_dummies.html){target="_blank"}

[Tutoriel Vidéo ](https://www.youtube.com/watch?v=w2gu9Ah95l0){target="_blank"}

[Tutoriel Vidéo Minecraft Like](https://www.youtube.com/watch?v=vX4l-qozib8){target="_blank"}


 - - - - - 

 - Module **Pyrailroad** :  un outil pour produire des diagrammes syntaxiques ou railroad diagrams par Rafaël Lopez. 

[Pypi](https://pypi.org/project/pyrailroad/){target="_blank"} et [Dépôt](https://github.com/Epithumia/pyrailroad){target="_blank"}

 - - - - - 

 - Module **ColabTurtleArbreBinaire** : représenter un arbre binaire et  visualiser son parcours par  Matthieu Bensussan

[Pypi](https://pypi.org/project/ColabTurtleArbreBinaire/){target="_blank"}


- - - - - -

- Module **Graphviz** :

[pypi.org](https://pypi.org/project/graphviz/){target="_blank"} création et visualisation de graphes 

[User Guide](https://graphviz.readthedocs.io/en/stable/manual.html){target="_blank"} et  [Developpez.com](https://cyberzoide.developpez.com/graphviz/){target="_blank"}



- - - - - -

- Module **Folium** : concevoir une carte affichée sur une page web [Site officiel](https://python-visualization.github.io/folium/){target="_blank"}

[Labo de Maths](http://labodemaths.fr/WordPress3/nsi-tp10-decouverte-du-module-folium-et-tables-de-donnees/){target="_blank"}  et [Site SIN ISN](http://sti2d-sin-isn.blogspot.com/2020/02/utilisation-basique-de-folium-sous.html){target="_blank"}

- - - - - -

- Module **Doctest** : un framework de test unitaire qui fait partie de la bibliothèque standard de Python [Documentation](https://docs.python.org/3/library/doctest.html){target="_blank"}

[Fabien Loison : Flozz Blog](https://blog.flozz.fr/2020/06/15/doctest-vous-navez-aucune-excuse-pour-ne-pas-ecrire-des-tests-unitaires-en-python/){target="_blank"}




- - - - - 

- Module **scikit-learn** :


[site officiel](https://scikit-learn.org/stable/index.html){target="_blank"} un outil simple et efficace pour faire de l'analyse prédictive de données


