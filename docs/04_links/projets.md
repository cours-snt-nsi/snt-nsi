title: projets

# Des Projets

[Ressources  de Anaïs Kobsch](https://github.com/Astranais/TimelineNumerique){target="_blank"}  version numérique du jeu TimeLine Histoire de l’Informatique

[Ressources  de David Landry](https://forge.apps.education.fr/nsi-clemenceau/nsi-1ere/-/tree/master/Projets?ref_type=heads){target="_blank"}  des projets classiques (bataille navale, mastermind, démineur, yam's, tic tac toe, pendu, tennis, Flappy bird), mais également originaux basés sur le thème « Harry Potter » 

[Ressources de Tristan LEY](https://trieles.gitlab.io/nsi/){target="_blank"} Jeu du démineur, puissance 4, introduction à Tkinter


[Moteur de recherche retro-gaming](https://picassciences.com/2021/04/02/moteur-de-recherche-retro-gaming-methode-de-boyer-moore-horspool/){target="_blank"}   méthode de Boyer-Moore-Horspool

[Ressources aulery](https://github.com/aulery/NSI_labyrinthe){target="_blank"} TP jeu du labyrinthe


[Charles Bouton](https://www.pearltrees.com/s/file/preview/218593863/Bouton1901.pdf?pearlId=301549059){target="_blank"} jeu de nim

[Ressources](https://sebhoa.gitlab.io/lupy/){target="_blank"}  idées ludiques pour faire programmer les élèves (Pac Man, Labyrinthe, Shikaku...)


[Codes-Sources](https://codes-sources.commentcamarche.net/source/list/python-19/340-jeux/last){target="_blank"} codes jeux Python

[Les algoblobs](https://algoblobs.educinfo.org/){target="_blank"} concept informatique développé pour approcher de plus en plus de comportements que l'on pourrait qualifier d'intelligent 


[E-LearningScape Info](https://webia.lip6.fr/~muratetm/elearningscape/){target="_blank"} Un jeu de type escape game sur le thème de l'informatique.



[Puissance4 et Chat](https://nsinotredame.gitlab.io/premierensi/Projets/projets/){target="_blank"} projet de 1NSI du lycée Notre Dame 


[Ressources MessageCrypt](https://github.com/CupOfCoffeeX/MessageCrypt){target="_blank"} client/serveur

[Jeu des 7 familles](https://interstices.info/jeu-de-7-familles-de-linformatique/){target="_blank"} Jeu de 7 familles de l’informatique 

[rosettacode](https://www.rosettacode.org/wiki/Bulls_and_cows/Player){target="_blank"} jeu Bulls and Cows version player






[Turtle Race](https://projects.raspberrypi.org/en/projects/turtle-race){target="_blank"} Projet Turtle Race avec Rasberry Py 



[Simon Tatham's Portable Puzzle Collection](https://www.chiark.greenend.org.uk/~sgtatham/puzzles/){target="_blank"}  One-player puzzle games
