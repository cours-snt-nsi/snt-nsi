---
author: Steeve PYTEL
title: 📋 Données en tables
---

!!! abstract "Présentation"

    À partir des types de base se constituent des types construits, qui sont introduits au fur et à mesure qu’ils sont nécessaires.     
    Il s’agit de présenter tour à tour les p-uplets (tuples), les enregistrements qui collectent des valeurs de types différents dans des champs nommés et les tableaux qui permettent un accès calculé direct aux éléments. En pratique, on utilise les  appellations de Python, qui peuvent être différentes de celles d’autres langages de programmation. 

|Contenus|Capacités attendues|
|:--- | :--- |
|Indexation de tables|Importer une table depuis un fichier texte tabulé ou un fichier CSV. |
|Recherche dans une table |Rechercher les lignes d’une table vérifiant des critères exprimés en logique propositionnelle.|
|Tri d’une table|Trier une table suivant une colonne.| 
|Fusion de tables|Construire une nouvelle table en combinant les données de deux tables|