---
author: Steeve PYTEL
title: 📋 Les Exercices
---

# Exercices


??? note "Programme de sciences numériques et technologie de seconde générale et technologique"

	<div class="centre">
	<iframe 
	src="/01_SNT/documents/BO.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

