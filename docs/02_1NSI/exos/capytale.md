---
author: Steeve PYTEL
title: 📋 CAPYTALE
---

# Exercices

!!! abstract "Intro Programmation"

    1. [tp1 : Les variables](https://capytale2.ac-paris.fr/web/c/b929-1767096){target='_blank'}
    2. [tp2 : Les instructions conditionnelles](https://capytale2.ac-paris.fr/web/c/4078-1767130){target='_blank'}
    3. [tp3 : Les boucles](https://capytale2.ac-paris.fr/web/c/92df-1767131){target='_blank'}
    4. [tp4 : Les fonctions](https://capytale2.ac-paris.fr/web/c/b0aa-1767132){target='_blank'}
    5. [tp5 : Les chaînes de caractères](https://capytale2.ac-paris.fr/web/c/97e5-1767134){target='_blank'}

!!! abstract "Intro Algorithme"

    1. [tp1](https://capytale2.ac-paris.fr/web/c/05f0-554046){target='_blank'}
    2. [tp2](https://capytale2.ac-paris.fr/web/c/419b-554047){target='_blank'} 
    3. [tp3](https://capytale2.ac-paris.fr/web/c/2b5d-554049){target='_blank'}

