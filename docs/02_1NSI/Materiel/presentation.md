---
author: Steeve PYTEL
title: 💾 Matérielles et OS
---

# Architectures matérielles et systèmes d’exploitation


!!! abstract "Présentation"

    Exprimer un algorithme dans un langage de programmation a pour but de le rendre exécutable par une machine dans un contexte donné. La découverte de l’architecture des machines et de leur système d’exploitation constitue une étape importante.  
    Les circuits électroniques sont au cœur de toutes les machines informatiques.Les réseaux permettent de transmettre l’information  entre machines. Les systèmes d’exploitation gèrent et optimisent l’ensemble des fonctions de la machine, de l’exécution des programmes aux entrées-sorties et à la gestion d’énergie.  
    On étudie aussi le rôle des capteurs et actionneurs dans les entrées-sorties clavier, interfaces graphiques et tactiles, dispositifs de mesure physique, commandes de machines, etc.  

|Contenus|Capacités attendues|
|:--- | :--- |
|Modèle d’architecture séquentielle (von Neumann)|Distinguer les rôles et les caractéristiques des différents constituants d’une machine. Dérouler l’exécution d’une séquence d’instructions simples du type langage machine.|
|Transmission de données dans un réseau Protocoles de communication Architecture d’un réseau|Mettre en évidence l’intérêt du découpage des données en paquets et de leur encapsulation. Dérouler le fonctionnement d’un protocole simple de récupération de perte de paquets (bit alterné). Simuler ou mettre en œuvre un réseau.|
|Systèmes d’exploitation |Identifier les fonctions d’un système d’exploitation. Utiliser les commandes de base en ligne de commande. Gérer les droits et permissions d’accès aux fichiers.|
|Périphériques d’entrée et de sortie Interface HommeMachine (IHM)|Identifier le rôle des capteurs et actionneurs. Réaliser par programmation une IHM répondant à un cahier des charges donné.|