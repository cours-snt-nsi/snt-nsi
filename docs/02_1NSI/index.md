---
title: 🙋 Enseigner 1er NSI
hide:
    -toc
---

# Enseigner la NSI pour préparer l'avenir 

!!! abstract "Présentation"

    La spécialité Numérique et Sciences Informatiques (NSI) en classe de première générale a pour objectif de permettre aux élèves de comprendre les concepts fondamentaux de l'informatique et du numérique. Elle leur offre une introduction approfondie aux enjeux et aux technologies du numérique, tout en développant leur capacité d'abstraction et leur esprit critique face à ces évolutions.!!! 
    
!!! abstract "Programme de la spécialité NSI en première"

    Le programme de NSI se compose des thèmes suivants :

    1. [Thème 1 - Histoire de l'informatique](./Histoire/presentation.md)
    2. [Thème 2 - Représentation des données : types et valeurs de base](./Donnees/types_de_base.md)
    3. [Thème 3 - Représentation des données : types construits](./Donnees/types_construits.md)
    4. [Thème 4 - Traitement de données en tables](./Donnees/tables.md)
    5. [Thème 5 - Interactions entre l'homme et la machine sur le Web](./Web/presentation.md)
    6. [Thème 6 - Architectures matérielles et systèmes d'exploitation](./Materiel/presentation.md)
    7. [Thème 7 - Langages et programmation](./Programmation/presentation.md)
    8. [Thème 8 - Algorithmique](./Algorithmique/presentation.md)

??? note "Programme de la spécialité NSI en première"

	<div class="centre">
	<iframe 
	src="./documents/BO.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


!!! abstract "Objectifs de la spécialité NSI en première"

    La spécialité NSI vise principalement à permettre aux élèves de :

    * Comprendre l'évolution historique et les concepts fondamentaux de l'informatique
    * Maîtriser les principes de représentation et de traitement des données numériques
    * Développer des compétences de programmation et de résolution de problèmes
    * Appréhender les interactions entre l'homme et la machine sur le Web
    * Analyser les architectures matérielles et logicielles des systèmes informatiques


!!! tip "Environ 30% du temps de la spécialité est consacré à des activités de programmation et de manipulation de systèmes informatiques en mode projet."

!!! question "Poursuites d'études après la spécialité NSI"

    Les élèves ayant suivi la spécialité NSI en première pourront envisager de poursuivre en terminale :

    * Bac Général avec la spécialité Numérique et Sciences Informatiques (NSI)
    * Bac Technologique avec la série Sciences et Technologies de l'Industrie et du Développement Durable (STI2D)
    * Bac Professionnel dans les domaines de l'informatique, du numérique et des technologies
    * Classes préparatoires scientifiques (MPSI, PCSI, etc.)
    * BTS, DUT et licences dans les secteurs de l'informatique, du multimédia, des télécommunications

!!! success "La spécialité NSI offre une solide formation aux sciences informatiques, ouvrant la voie à de nombreuses poursuites d'études supérieures dans ces secteurs en forte croissance."
