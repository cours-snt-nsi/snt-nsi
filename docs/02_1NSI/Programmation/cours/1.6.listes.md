---
author: Steeve PYTEL
title: 📚 listes
---

# La Photographie

## Introduction

!!! abstract "Présentation"

    Dans cette activité, les élèves visionnent une vidéo d’introduction à la photographie numérique et rédigent un texte pour résumer ce qu’ils ont compris.



??? note "01. Introduction"

	<div class="centre">
	<iframe 
	src="../a_telecharger/01/01. Introduction - PHOTOS NUMERIQUES.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[01. Introduction](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Photo/a_telecharger/01){ .md-button target="_blank" rel="noopener" }