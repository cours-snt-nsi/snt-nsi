---
author: Steeve PYTEL
title: 💻 Programmation
---

# Langages et programmation


!!! abstract "Présentation"

    Le travail entrepris en classe de première sur les méthodes de programmation est prolongé. L’accent est mis sur une programmation assurant une meilleure sûreté, c’est-à-dire minimisant le nombre d’erreurs. Parallèlement, on montre l’universalité et les limites de la notion de calculabilité.  
    La récursivité est une méthode fondamentale de programmation. Son introduction permet également de diversifier les algorithmes étudiés. En classe terminale, les élèves s’initient à différents paradigmes de programmation pour ne pas se limiter à une démarche impérative. 

|Contenus|Capacités attendues|
|:--- | :--- |
|Notion de programme en tant que donnée. Calculabilité, décidabilité.|Comprendre que tout programme est aussi une donnée. Comprendre que la calculabilité ne dépend pas du langage de programmation utilisé. Montrer, sans formalisme théorique, que le problème de l’arrêt est indécidable.|
|Récursivité.|Écrire un programme récursif. Analyser le fonctionnement d’un programme récursif.|
|Modularité.|Utiliser des API (Application Programming Interface) ou des bibliothèques. Exploiter leur documentation. Créer des modules simples et les documenter.|
|Paradigmes de programmation.|Distinguer sur des exemples les paradigmes impératif, fonctionnel et objet. Choisir le paradigme de programmation selon le champ d’application d’un programme.|
|Mise au point des programmes. Gestion des bugs.|Dans la pratique de la programmation, savoir répondre aux causes typiques de bugs : problèmes liés au typage, effets de bord non désirés, débordements dans les tableaux, instruction conditionnelle non exhaustive, choix des inégalités, comparaisons et calculs entre flottants, mauvais nommage des variables, etc.|