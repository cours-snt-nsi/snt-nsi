---
title: Présentation
hide:
    -toc
    -navigation
---

!!! abstract "Bienvenue sur notre plateforme de cours en ligne !"
    ## Découvrez nos formations adaptées aux élèves de lycée :

    ## [Sciences Numériques et Technologie (SNT)](./01_SNT/index.md)
    Explorez les concepts fondamentaux du numérique et de la technologie à travers des cours vidéo, des exercices pratiques et des projets stimulants. Développez vos compétences en programmation, en analyse de données et en cybersécurité.

    ## [Numérique et Sciences Informatiques (NSI) - Première](./02_1NSI/index.md)
    Plongez dans les profondeurs de l'informatique avec notre programme riche et diversifié. Explorez les fondements de l'algorithmique, la structure et le fonctionnement des ordinateurs. Approfondissez vos connaissances sur les différents aspects de l'informatique, de la théorie à la pratique. Préparez-vous à comprendre les mécanismes complexes qui se cachent derrière les technologies numériques d'aujourd'hui.

    ## [Numérique et Sciences Informatiques (NSI) - Terminale](./03_TNSI/index.md)
    Poussez plus loin votre compréhension de l'informatique et de ses applications. Découvrez les réseaux, les bases de données, l'intelligence artificielle et bien d'autres sujets passionnants. Devenez un as de l'informatique grâce à notre formation de haute qualité.

