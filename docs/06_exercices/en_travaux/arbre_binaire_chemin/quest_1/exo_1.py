

# --------- PYODIDE:code --------- #

def longueur_ce(arbre, noeud, longueur):
    ...

# --------- PYODIDE:corr --------- #

def longueur_ce(arbre, noeud, longueur):
    if noeud in arbre:
        longueur_g = longueur_ce(arbre, arbre[noeud][0], longueur+1)
        longueur_d = longueur_ce(arbre, arbre[noeud][1], longueur+1)
        return longueur_g + longueur_d
    else:
        return longueur

# --------- PYODIDE:tests --------- #

arbre = {0: [1, 2], 1: [4, 5], 2: [6, 3], 3: [7, 8]}
assert longueur_ce(arbre, 0, 0) == 12

# --------- PYODIDE:secrets --------- #


#tests secrets