On peut aussi définir la longueur de chemin interne comme la somme des longueurs de tous les chemins allant de la racine à un nœud interne.

C'est un peu plus compliqué.