

# --------- PYODIDE:code --------- #

def longueur_ponderee(arbre, poids, noeud, longueur):
    ...

# --------- PYODIDE:corr --------- #

def longueur_ponderee(arbre, poids, noeud, longueur):
    if noeud in arbre:
        longueur_g = longueur_ponderee(arbre, poids, arbre[noeud][0], longueur+1)
        longueur_d = longueur_ponderee(arbre, poids, arbre[noeud][1], longueur+1)
        return longueur_g + longueur_d
    else:
        return longueur * poids[noeud]

# --------- PYODIDE:tests --------- #

arbre = {0: [1, 2], 1: [3, 4], 2: [5, 6]}
poids = {3: 4, 4: 1, 5: 9, 6: 2}
assert longueur_ponderee(arbre, poids, 0, 0) == 32

# --------- PYODIDE:secrets --------- #

# tests
arbre = {0: [1, 2], 1: [3, 4], 4: [5, 6]}
poids = {2: 11, 3: 4, 5: 2, 6: 3}
assert longueur_ponderee(arbre, poids, 0, 0) == 34
#tests secrets
arbre = {0: [1, 2], 2: [3, 4], 4: [5, 6]}
poids = {1: 9, 3: 4, 5: 1, 6: 2}
assert longueur_ponderee(arbre, poids, 0, 0) == 26

