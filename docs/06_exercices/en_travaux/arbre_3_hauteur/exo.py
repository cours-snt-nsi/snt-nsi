

# --------- PYODIDE:code --------- #

def profondeurs(arbre):
    memo = {}
    for n in range(len(arbre)):
        if n not in ...:
            branche = [n]
            enfant = n
            parent = arbre[...]
            while parent != ... and parent not in ...:
                branche.append(parent)
                enfant = parent
                parent = ...
            if parent == ...:  # cas de la racine
                for i in range(...):
                    memo[branche[i]] = ...
            else:
                for i in range(...):
                    memo[branche[i]] = ...
    return ...

def hauteur(arbre):
    dic_profondeurs = ...
    profondeur_max = ...
    for profondeur in ...:
        if ...:
            profondeur_max = ...
    return ...

# --------- PYODIDE:corr --------- #

def profondeurs(arbre):
    memo = {}
    for n in range(len(arbre)):
        if n not in memo:
            branche = [n]
            enfant = n
            parent = arbre[enfant]
            while parent != enfant and parent not in memo:
                branche.append(parent)
                enfant = parent
                parent = arbre[enfant]
            if parent == enfant:  # c'est la racine
                for i in range(len(branche)):
                    memo[branche[i]] = len(branche) - 1 - i
            else:
                for i in range(len(branche)):
                    memo[branche[i]] = len(branche) + memo[parent] - i
    return memo

def hauteur(arbre):
    dic_profondeurs = profondeurs(arbre)
    profondeur_max = 0
    for profondeur in dic_profondeurs.values():
        if profondeur > profondeur_max:
            profondeur_max = profondeur
    return profondeur_max

# --------- PYODIDE:tests --------- #

arbre = [0]
assert profondeurs([0]) == {0: 0}
assert hauteur(arbre) == 0
arbre = [1, 1]
assert profondeurs([1, 1]) == {0: 1, 1: 0}
assert hauteur(arbre) == 1
arbre = [1, 2, 2, 2]
assert profondeurs([1, 2, 2, 2]) == {0: 2, 1: 1, 2: 0, 3: 1}
assert hauteur(arbre) == 2

# --------- PYODIDE:secrets --------- #


#tests secrets
arbre = [0, 0, 0, 0, 1, 3, 3, 3]
assert profondeurs([0, 0, 0, 0, 1, 3, 3, 3]) == {0: 0, 1: 1, 2: 1, 3: 1, 4: 2, 5: 2, 6: 2, 7: 2}
assert hauteur(arbre) == 2
arbre = [4, 0, 6, 1, 4, 4, 5, 5]
assert hauteur(arbre) == 3
arbre = a = [0, 0, 1, 2, 6, 3, 5]
assert hauteur(arbre) == 6

arbre = [0] * 10**5
assert hauteur(arbre) == 1
arbre = [i+1 for i in range(10**4)] + [10**4]
assert hauteur(arbre) == 10**4
arbre = [1, 1] + [i for i in range(1, 10**4)]
assert hauteur(arbre) == 10**4-1