---
author:
    - Guillaume Connan
    - Nicolas Revéret
    - Pierre Marquestaut
hide:
    - navigation
    - toc
title: Degré de séparation
difficulty: 220
tags:
    - dictionnaire
    - graphe
    - file
maj: 06/06/2024
---

On considère le schéma suivant où une flèche allant d'une personne Α vers une
personne B indique que la personne A « suit » la personne B sur son compte
*Immediam*. On dit alors que B est un *ami* de A.

??? example "Voici un exemple de réseau *Immediam*"
    ```mermaid
    flowchart LR
        A([Anna]) --> B([Billy])
        B --> A
        B --> E([Eroll])
        C([Carl]) --> B
        D([Dora]) --> G([Gaby])
        E --> B
        E --> D
        E --> G
        E --> F([Flynn])
        F --> G
        G --> E
    ```
    Les amis d'Eroll sont Dora, Gaby, Flynn et Billy.

    Les amis des amis d'Eroll sont donc Gaby (amie à la fois de Dora et Flynn) et Anna (amie de Billy).

!!! abstract "Degré de séparation"
    Dans un réseau social, le degré de séparation est le nombre de relations entre deux individus du réseau.

    Ainsi, il y a $1$ degré de séparation entre Billy et Eroll, et $3$ degrés entre Carl et Flynn.


On représente ce réseau *Immediam* en machine par un dictionnaire dans lequel :

- les clés sont les chaînes de caractères correspondant aux noms des personnes inscrites, 
  
- les valeurs associées sont des listes de chaînes de caractères représentant les personnes suivies.


Écrire une fonction  `degres_separation` qui :

- prend en argument un dictionnaire `reseau` représentant un tel réseau *Immediam* et deux chaînes de caractères
`membre1` et `membre2` qui représentent deux membres du réseau ;

- renvoie un entier indiquant les degrés de séparation entre les deux membres. 

On garentie qu'il existe un chemin valide entre le membre1 et le membre2.

??? tip "Aide (1)"

    On pourra utiliser un parcours particulier du graphe. À ce titre, on fournit une la classe `File` dont on donne l'interface ci-dessous.



??? tip "Aide (2)"

    On pourra stocker des couples (membre, degre_de_separation).


???+ example "Exemples"

    ```pycon title=""
    >>> immediam = {
            "Anna": ["Billy"],
            "Billy": ["Anna", "Eroll"],
            "Carl": ["Billy"],
            "Dora": ["Gaby"],
            "Eroll": ["Billy", "Dora", "Flynn", "Gaby"],
            "Flynn": ["Gaby"],
            "Gaby": ["Eroll"],
    }
    >>> degres_separation(immediam, 'Billy', 'Eroll')
	1
    >>> degres_separation(immediam, 'Carl', 'Flynn')
	3
	>>> degres_separation(immediam, 'Eroll', 'Gaby')
	1
    ```

{{ IDE('exo') }}
