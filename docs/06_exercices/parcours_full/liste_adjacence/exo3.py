

# --------- PYODIDE:code --------- #

def dict_vers_matrice(dictionnaire):
    n = len(dictionnaire)
    matrice = ...
    for sommet in ...:
        for successeur in ...:
            ...
    return matrice

# --------- PYODIDE:corr --------- #

def dict_vers_matrice(dictionnaire):
    n = len(dictionnaire)
    matrice = [n * [0] for i in range(n)]
    for sommet in dictionnaire:
        for successeur in dictionnaire[sommet]:
            matrice[sommet][successeur] = 1
    return matrice

# --------- PYODIDE:tests --------- #

d = {0: [1, 2], 1: [0, 2, 3], 2: [0, 1, 3], 3: [1, 2]}
assert dict_vers_matrice(d) == [[0, 1, 1, 0], [1, 0, 1, 1], [1, 1, 0, 1], [0, 1, 1, 0]]
d = {0: [1], 1: [0]}
assert dict_vers_matrice(d) == [[0, 1], [1, 0]]
d = {0: [], 1: [0]}
assert dict_vers_matrice(d) == [[0, 0], [1, 0]]
d = {0: [], 1: []}
assert dict_vers_matrice(d) == [[0, 0], [0, 0]]
d = {0: []}
assert dict_vers_matrice(d) == [[0]]
d = {0: [0]}
assert dict_vers_matrice(d) == [[1]]

# --------- PYODIDE:secrets --------- #


# tests secrets
m = [[0, 1, 1, 0], [1, 0, 1, 1], [1, 1, 0, 1], [0, 1, 1, 0]]
d = {2: [0, 1, 3], 1: [0, 2, 3], 3: [1, 2], 0: [1, 2]}
assert dict_vers_matrice(d) == m, "Erreur avec d = {2: [0, 1, 3], 1: [0, 2, 3], 3: [1, 2], 0: [1, 2]}"
m = [[0] * 10**3 for i in range(10**3)]
d = {i: [] for i in range(10**3)}
assert dict_vers_matrice(d) == m, "Erreur avec d = {i: [] for i in range(10**3)}"
m = [[1] * 10**3 for i in range(10**3)]
d = {i: [k for k in range(10**3)] for i in range(10**3)}
assert dict_vers_matrice(d) == m, "Erreur avec d = {i: [k for k in range(10**3)] for i in range(10**3)}"
m = [[[0, 1][i%2]] * 10**3 for i in range(10**3)]
d = {i: [k for k in range(10**3)] if i%2 else [] for i in range(10**3)}
assert dict_vers_matrice(d) == m, "Erreur avec d = {i: [k for k in range(10**3)] if i%2 else [] for i in range(10**3)}"