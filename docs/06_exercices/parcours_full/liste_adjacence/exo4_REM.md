Dans le cas d'un graphe représenté par un dictionnaire dont les sommets ne sont pas numérotés, on commence par numéroter les sommets avant de pouvoir utiliser les fonctions de l'exercice.

Par exemple:

```python 
G = {"A" : ["B", "D"], "B" : ["A", "C", "D"], "C" : ["B"], "D" : ["A", "B"]}

def numerote(g):
    numeros = {}
    n = 0
    for sommet in g:
        numeros[sommet] = n
        n = n + 1
    return numeros
```

On obtient le dictionnaire `{"A": 0, "B": 1, "C": 2, "D": 3}`

À partir du dictionnaire des numéros de sommets, `numeros`, on récupère les noms des sommets avec : 

```python title=""
sommets = {valeur: cle for (cle, valeur) in numeros.items()}
```
