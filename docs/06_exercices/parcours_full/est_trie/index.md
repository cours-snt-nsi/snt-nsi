---
author: Sébastien Hoarau
hide:
    - navigation
    - toc
title: Est trié ?
tags:
    - booléen
    - tri
    - liste/tableau
    - ep1
difficulty: 180
---

Programmer la fonction `est_trie` qui prend en paramètre un tableau de valeurs numériques et qui détermine si ce tableau est trié dans l'ordre croissant. La fonction renvoie un booléen `True` ou `False`.

???+ example "Exemples"

    ```pycon title=""
    >>> est_trie([0, 5, 8, 8, 9])
    True
    >>> est_trie([8, 12, 4])
    False
    >>> est_trie([-1, 4])
    True
    >>> est_trie([5])
    True
    >>> est_trie([])
    True
    ```

{{ IDE('exo', SANS=".sort, sorted") }}
