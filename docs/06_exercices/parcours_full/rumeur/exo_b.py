

# --------- PYODIDE:env --------- #

from collections import deque


class File:
    """Classe définissant une structure de file"""

    def __init__(self):
        self.valeurs = deque([])

    def est_vide(self):
        """Renvoie le booléen True si la file est vide, False sinon"""
        return len(self.valeurs) == 0

    def enfile(self, x):
        """Place x à la queue de la file"""
        self.valeurs.append(x)

    def defile(self):
        """Retire et renvoie l'élément placé à la tête de la file.
        Provoque une erreur si la file est vide
        """
        if self.est_vide():
            raise ValueError("La file est vide")
        return self.valeurs.popleft()

# --------- PYODIDE:code --------- #

def distance(graphe, origine, destination):
    file = File()
    au_courant = {personne: ... for personne in ...}
    file.enfile((..., ...))
    while not ...:
        ..., ... = file.defile()
        au_courant[...] = ...
        if ... == ...:
            return ...
        for voisin in graphe[...]:
            if not au_courant[...]:
                file.enfile((..., ... + 1))
    return ...

# --------- PYODIDE:corr --------- #

def distance(graphe, origine, destination):
    file = File()
    au_courant = {personne: False for personne in graphe}
    file.enfile((origine, 0))
    while not file.est_vide():
        actuel, nb_etapes = file.defile()
        au_courant[actuel] = True
        if actuel == destination:
            return nb_etapes
        for voisin in graphe[actuel]:
            if not au_courant[voisin]:
                file.enfile((voisin, nb_etapes + 1))
    return None

# --------- PYODIDE:tests --------- #

graphe = {
    "Camille": ["Romane", "Marion", "Paul"],
    "Romane": ["Nicolas", "Antoine", "Paul"],
    "Marion": ["Camille", "Romane"],
    "Paul": ["Antoine", "Romane"],
    "Antoine": ["Nicolas"],
    "Nicolas": ["Camille", "Antoine"],
    "Stéphane": ["Antoine"],
}

assert distance(graphe, "Romane", "Romane") == 0
assert distance(graphe, "Romane", "Antoine") == 1
assert distance(graphe, "Romane", "Marion") == 3
assert distance(graphe, "Romane", "Stéphane") is None

# --------- PYODIDE:secrets --------- #


# tests secrets
graphe = {
    "Camille": ["Romane", "Marion", "Paul"],
    "Romane": ["Nicolas", "Antoine", "Paul"],
    "Marion": ["Camille", "Romane"],
    "Paul": ["Antoine", "Romane"],
    "Antoine": ["Nicolas"],
    "Nicolas": ["Camille", "Antoine"],
    "Stéphane": ["Antoine"],
}

assert distance(graphe, "Camille", "Camille") == 0, f"Erreur dans {graphe = }"
assert distance(graphe, "Camille", "Romane") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Camille", "Marion") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Camille", "Paul") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Camille", "Antoine") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "Camille", "Nicolas") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "Camille", "Stéphane") is None, f"Erreur dans {graphe = }"
assert distance(graphe, "Romane", "Camille") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "Romane", "Romane") == 0, f"Erreur dans {graphe = }"
assert distance(graphe, "Romane", "Marion") == 3, f"Erreur dans {graphe = }"
assert distance(graphe, "Romane", "Paul") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Romane", "Antoine") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Romane", "Nicolas") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Romane", "Stéphane") is None, f"Erreur dans {graphe = }"
assert distance(graphe, "Marion", "Camille") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Marion", "Romane") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Marion", "Marion") == 0, f"Erreur dans {graphe = }"
assert distance(graphe, "Marion", "Paul") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "Marion", "Antoine") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "Marion", "Nicolas") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "Marion", "Stéphane") is None, f"Erreur dans {graphe = }"
assert distance(graphe, "Paul", "Camille") == 3, f"Erreur dans {graphe = }"
assert distance(graphe, "Paul", "Romane") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Paul", "Marion") == 4, f"Erreur dans {graphe = }"
assert distance(graphe, "Paul", "Paul") == 0, f"Erreur dans {graphe = }"
assert distance(graphe, "Paul", "Antoine") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Paul", "Nicolas") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "Paul", "Stéphane") is None, f"Erreur dans {graphe = }"
assert distance(graphe, "Antoine", "Camille") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "Antoine", "Romane") == 3, f"Erreur dans {graphe = }"
assert distance(graphe, "Antoine", "Marion") == 3, f"Erreur dans {graphe = }"
assert distance(graphe, "Antoine", "Paul") == 3, f"Erreur dans {graphe = }"
assert distance(graphe, "Antoine", "Antoine") == 0, f"Erreur dans {graphe = }"
assert distance(graphe, "Antoine", "Nicolas") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Antoine", "Stéphane") is None, f"Erreur dans {graphe = }"
assert distance(graphe, "Nicolas", "Camille") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Nicolas", "Romane") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "Nicolas", "Marion") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "Nicolas", "Paul") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "Nicolas", "Antoine") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Nicolas", "Nicolas") == 0, f"Erreur dans {graphe = }"
assert distance(graphe, "Nicolas", "Stéphane") is None, f"Erreur dans {graphe = }"
assert distance(graphe, "Stéphane", "Camille") == 3, f"Erreur dans {graphe = }"
assert distance(graphe, "Stéphane", "Romane") == 4, f"Erreur dans {graphe = }"
assert distance(graphe, "Stéphane", "Marion") == 4, f"Erreur dans {graphe = }"
assert distance(graphe, "Stéphane", "Paul") == 4, f"Erreur dans {graphe = }"
assert distance(graphe, "Stéphane", "Antoine") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "Stéphane", "Nicolas") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "Stéphane", "Stéphane") == 0, f"Erreur dans {graphe = }"

graphe = {"A": ["B"], "B": ["C"], "C": []}

assert distance(graphe, "A", "B") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "A", "C") == 2, f"Erreur dans {graphe = }"
assert distance(graphe, "B", "C") == 1, f"Erreur dans {graphe = }"
assert distance(graphe, "C", "B") is None, f"Erreur dans {graphe = }"