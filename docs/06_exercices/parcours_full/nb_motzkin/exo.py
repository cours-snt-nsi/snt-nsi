

# --------- PYODIDE:code --------- #

motzkin_mem = [1]

def motzkin(n):
    if n >= len(motzkin_mem):
        resultat = motzkin(...)
        for i in range(...):
            resultat += motzkin(...) ... motzkin(...)
        # motzkin_mem est ici de longueur n
        motzkin_mem.append(resultat)
        # et là de longueur n + 1
    return motzkin_mem[n]

# --------- PYODIDE:corr --------- #

motzkin_mem = [1]


def motzkin(n):
    if n >= len(motzkin_mem):
        resultat = motzkin(n - 1)
        for i in range(1, n):
            resultat += motzkin(i - 1) * motzkin(n - 1 - i)
        # motzkin_mem est ici de longueur n
        motzkin_mem.append(resultat)
        # et là de longueur n + 1
    return motzkin_mem[n]

# --------- PYODIDE:tests --------- #

assert motzkin(4) == 9
assert motzkin(5) == 21

# --------- PYODIDE:secrets --------- #


# autres tests

A001006 = [
    1,
    1,
    2,
    4,
    9,
    21,
    51,
    127,
    323,
    835,
    2188,
    5798,
    15511,
    41835,
    113634,
    310572,
    853467,
    2356779,
    6536382,
    18199284,
    50852019,
    142547559,
    400763223,
    1129760415,
    3192727797,
    9043402501,
    25669818476,
    73007772802,
    208023278209,
    593742784829,
]

for n, resultat in enumerate(A001006):
    assert motzkin(n) == resultat, f"Erreur avec n = {n}"