Autre méthode possible :  

On peut créer un dictionnaire d'occurences dont les clés sont tous les caractères de `phrase`, et les valeurs associées : `0`. Cela se fait facilement en compréhension.   
Il suffit ensuite de parcourir la chaine de caractères `phrase`, et d'actualiser le dictionnaire.

```python
def occurrence_caracteres(phrase):
    occurrences = {lettre : 0 for lettre in phrase}
    for lettre in phrase:
        occurrences[lettre] = occurrences[lettre] + 1
    return occurrences
```
