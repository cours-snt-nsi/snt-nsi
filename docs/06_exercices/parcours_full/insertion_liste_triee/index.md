---
author: Franck Chambon
hide:
    - navigation
    - toc
title: Insère dans liste triée
tags:
    - à trous
    - tri
    - ep2
difficulty: 210
---

# Insertion dans une liste triée

On considère la fonction `insere` ci-dessous qui prend en argument un entier `a` et une liste `nombres` d'entiers triés par ordre croissant. Cette fonction insère la valeur `a` dans la liste de sorte que la liste reste triée.


???+ example "Exemples"

    ```pycon title=""
    >>> exemple_1 = [1, 2, 4, 5]
    >>> insere(3, exemple_1)
    >>> exemple_1
    [1, 2, 3, 4, 5]
    ```

    ```pycon title=""
    >>> exemple_2 = [1, 2, 7, 12, 14, 25]
    >>> insere(7, exemple_2)
    >>> exemple_2
    [1, 2, 7, 7, 12, 14, 25]
    ```

    ```pycon title=""
    >>> exemple_3 = [2, 3, 4]
    >>> insere(1, exemple_3)
    >>> exemple_3
    [1, 2, 3, 4]
    ```

Compléter la fonction `insere` ci-dessous.

{{ IDE('exo') }}
