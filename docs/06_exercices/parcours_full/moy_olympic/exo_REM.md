## Boucle unique

Pour résoudre cet exercice, il fallait rechercher les valeurs minimale et maximale, puis calculer la moyenne.

Il est possible d'utiliser trois boucles, mais la solution proposée avec une seule boucle permet de minimiser le nombre d'instructions.

En revanche, dans les deux cas, la complexité de l'algorithme reste linaire.

## Initialisation du minimum et du maximum

Dans le corrigé, on initialise le minimum et le maximum avec la première valeur du tableau qui est garanti non vide ici.

Les notes étant comprises entre 0 et 10, on aurait pu également se servir de ces valeurs pour l'initialisation sans crainte d'erreur.

```python
mini = 10
maxi = 0
```
