---
author:
    - Guillaume Connan
    - Pierre Marquestaut
hide:
    - navigation
    - toc
difficulty: 180
title: Moyenne olympique
tags:
    - liste/tableau
    - float
---

# Moyenne olympique

Le championnat d'Europe de la bourrée auvergnate a été organisé à Nantes.


![Bourrée](https://upload.wikimedia.org/wikipedia/commons/5/5a/Bourr%C3%A9e_d%27Auvergne.jpg){ width=35% .center title="source : https://upload.wikimedia.org/wikipedia/commons/5/5a/Bourr%C3%A9e_d%27Auvergne.jpg"}

A l'issue de sa chorégraphie, chaque athlète reçoit une note de chacun des $n$ juges, le nombre de juges étant obligatoirement supérieur à $3$.

La note finale est calculée selon la méthode de la **moyenne olympique** : on enlève les deux notes extrêmes et on calcule la moyenne des $n - 2$ notes restantes.

Les notes sont des nombres entiers entre `#!py 0` et `#!py 10` inclus l'un et l'autre.

Vous devez aider les organisateurs à donner la note finale en créant une fonction `moyenne_olympique` qui prend en paramètre un tableau contenant les notes d'au moins trois juges et renvoie la moyenne olympique attendue.

!!! warning "Contraintes"

    On n'utilisera pas les fonctions Python `max`, `min`, `sort`, `sorted` ni `sum`.


???+ example "Exemples"

    ```pycon title=""
	>>> notes_1 = [2, 0, 2, 10, 2]
	>>>  moyenne_olympique(notes_1)
    2.0
	>>> notes_2 = [1, 1, 1, 1, 1, 1]
	>>> moyenne_olympique(notes_2)
    1.0
    >>> notes_3 = [5, 1, 4, 3, 2, 6]
 	>>> moyenne_olympique(notes_3)
    3.5
    ```


{{ IDE('exo', SANS="min, max, sum, .sort, sorted") }}
