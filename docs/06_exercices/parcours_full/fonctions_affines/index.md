---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Fonctions affines
tags:
    - maths
    - float
    - fonctions
difficulty: 60
maj: 01/03/2024
---

Cette page regroupe différents exercices simples sur les fonctions affines. On rappelle qu'une fonction affine est une fonction définie pour tout nombre réel $x$ par :

$$f(x) = ax + b$$

Les exercices sont tous indépendants et peuvent être traités dans un ordre quelconque.

??? question "Définir la fonction"

    Le code contenu dans l'éditeur ci-dessous définit la fonction $f:x\mapsto 5x-9$.
    
    Ajouter en dessous le code définissant la fonction $g:x\mapsto -3x+7$.
      
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> g(0)  # quelle est l'image de 0 par g: x ↦ -3x + 7 ?
        7
        >>> g(10)  # quelle est l'image de 10 par g: x ↦ -3x  + 7 ?
        -23
        ```
    
    {{ IDE('./pythons/exo_fonction') }}

??? question "Sens de variations"

    Écrire la fonction `variations` qui prend en paramètres le coefficient directeur `a` et l'ordonnée à l'origine `b` d'une fonction affine $f$ et renvoie :
    
    * `#!py 'strictement croissante'` si la fonction $f$ est strictement croissante ;
    * `#!py 'constante'` si la fonction $f$ est constante ;
    * `#!py 'strictement décroissante'` si la fonction $f$ est strictement décroissante.

    On garantit que les paramètres `a` et `b` sont des entiers.

    ???+ example "Exemples"
    
        ```pycon title=""
        >>> variations(4, -8)  # comment varie f: x ↦ 4x - 8 ?
        'strictement croissante'
        >>> variations(-2, 15)  # comment varie g: x ↦ -2x + 15 ?
        'strictement décroissante'
        ```
    
    {{ IDE('./pythons/exo_variations') }}
    
??? question "Antécédent de $0$"

    On considère dans cette question des fonctions affines de coefficient directeur non nul.
    
    Écrire la fonction `antecedent_zero` qui prend en paramètres le coefficient directeur `a` et l'ordonnée à l'origine `b` d'une fonction affine et renvoie la valeur de l'antécédent de $0$.
          
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> antecedent_zero(4, -8)  # quel est l'antécédent de 0 par f: x ↦ 4x - 8 ?
        2.0
        >>> antecedent_zero(-2, 15)  # quel est l'antécédent de 0 par g: x ↦ -2x + 15 ?
        7.5
        ```
    
    {{ IDE('./pythons/exo_zero') }}

??? question "Antécédent de $y$"

    On considère dans cette question des fonctions affines de coefficient directeur non nul.
    
    Écrire la fonction `antecedent` qui prend en paramètres le coefficient directeur `a` et l'ordonnée à l'origine `b` d'une fonction affine $f$ ainsi qu'un nombre `y` et renvoie la valeur de l'antécédent de $y$ par $f$.
          
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> antecedent(4, -8, 5)  # quel est l'antécédent de 5 par f: x ↦ 4x - 8 ?
        3.25
        >>> antecedent(-2, 15, 5)  # quel est l'antécédent de 5 par g: x ↦ -2x + 15 ?
        5.0
        ```
    
    {{ IDE('./pythons/exo_antecedent') }}

??? question "Coefficient directeur"

    On considère dans cette question une fonction affine dont la représentation graphique $C_f$ passe par deux points $A\,(x_A\,;\,y_A)$ et $B\,(x_B\,;\,y_B)$.
    
    Écrire la fonction `coefficient_directeur` qui prend en paramètres les coordonnées `x_A`, `y_A`, `x_B` et `y_B` des points $A$ et $B$ et renvoie le coefficient directeur de $f$.

    On garantit que les abscisses $x_A$ et $x_B$ des deux points sont différentes.

    ???+ example "Exemples"
    
        ```pycon title=""
        >>> coefficient_directeur(1, 4, 9, 10)  # C_f passe par A (1 ; 4) et B (9 ; 10)
        0.75
        >>> coefficient_directeur(3, 15, -2, 15)  # C_f passe par A (3 ; 15) et B (-2 ; 15)
        0.0
        ```
    
    {{ IDE('./pythons/exo_coeff_dir') }}

??? question "Ordonnée à l'origine"

    On considère dans cette question une fonction affine dont la représentation graphique $C_f$ passe par deux points $A\,(x_A\,;\,y_A)$ et $B\,(x_B\,;\,y_B)$.
    
    Écrire la fonction `ordonnee_origine` qui prend en paramètres les coordonnées `x_A`, `y_A`, `x_B` et `y_B` des points $A$ et $B$ et renvoie l'ordonnée à l'origine de $f$.

    On garantit que les abscisses $x_A$ et $x_B$ des deux points sont différentes.
    
    La fonction `coefficient_directeur` de la question précédente est déjà chargée dans l'éditeur. Vous pouvez l'utiliser.
    
    ???+ example "Exemples"
    
        ```pycon title=""
        >>> ordonnee_origine(1, 4, 9, 10)  # C_f passe par A (1 ; 4) et B (9 ; 10)
        3.25
        >>> ordonnee_origine(3, 15, -2, 15)  # C_f passe par A (3 ; 15) et B (-2 ; 15)
        15.0
        ```
    
    {{ IDE('./pythons/exo_ord_origine') }}
