# --- PYODIDE:code --- #
def est_multiple(a, b):
    if b == ...:
        if ...:
            return ...
        else:
             return ...
    multiple = ...
    while ... < ...:
        multiple = ...
    if ...:
        return ...
    else:
        return ...


# --- PYODIDE:corr --- #
def est_multiple(a, b):
    if b == 0:
        if a == 0:
            return True
        else:
             return False
    multiple = 0
    while multiple < a:
        multiple = multiple + b
    if multiple == a:
        return True
    else:
        return False


# --------- PYODIDE:tests --------- #
assert est_multiple(0, 0) is True
assert est_multiple(5, 0) is False
assert est_multiple(10, 2) is True
assert est_multiple(10, 10) is True
assert est_multiple(2, 10) is False
assert est_multiple(7, 3) is False
# --------- PYODIDE:secrets --------- #
from random import randrange


for _ in range(20):
    a = randrange(1, 1000)
    b = randrange(1, 100)
    assert est_multiple(a, 0) is False, f"Erreur avec {a = } et {b = }"
    attendu = a % b == 0
    assert est_multiple(a, b) is attendu, f"Erreur avec {a = } et {b = }"
    b = a + 1
    assert est_multiple(a, b) is False, f"Erreur avec {a = } et {b = }"
