

# --------- PYODIDE:code --------- #

HEX_DEC = {
    "0": 0,
    "1": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "A": 10,
    "B": 11,
    # ...
}


def hex_int(seizaine, unite):
    ...



# --------- PYODIDE:corr --------- #

HEX_DEC = {
    "0": 0,
    "1": 1,
    "2": 2,
    "3": 3,
    "4": 4,
    "5": 5,
    "6": 6,
    "7": 7,
    "8": 8,
    "9": 9,
    "A": 10,
    "B": 11,
    "C": 12,
    "D": 13,
    "E": 14,
    "F": 15,
}


def hex_int(seizaine, unite):
    return HEX_DEC[seizaine] * 16 + HEX_DEC[unite]

# --------- PYODIDE:tests --------- #

assert hex_int("B", "5") == 181, "Échec hex_int exemple 1 de l'énoncé"
assert hex_int("0", "0") == 0, "Échec hex_int exemple 2 de l'énoncé"


# --------- PYODIDE:secrets --------- #


# tests supplémentaires
assert hex_int("F", "F") == 255, "Échec pour la valeur FF"
assert hex_int("8", "8") == 136, "Échec pour la valeur 88"
assert hex_int("7", "6") == 118, "Échec pour la valeur 76"
assert hex_int("0", "0") == 0, "Échec pour la valeur 00"
assert hex_int("1", "7") == 23, "Échec pour la valeur 17"

