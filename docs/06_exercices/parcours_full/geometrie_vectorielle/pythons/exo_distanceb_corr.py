from math import sqrt


def distance(x_A, y_A, x_B, y_B):
    return sqrt((x_B - x_A) ** 2 + (y_B - y_A) ** 2)


