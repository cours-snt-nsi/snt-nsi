

# --------- PYODIDE:code --------- #

def vecteur(x_A, y_A, x_B, y_B):
    ...

# --------- PYODIDE:corr --------- #

def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)

# --------- PYODIDE:tests --------- #

assert vecteur(0, 2, 3, 6) == (3, 4)
assert vecteur(10, 4, 5, -8) == (-5, -12)

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) / 2
    x_B = randrange(-200, 200) / 2
    y_A = randrange(-200, 200) / 2
    y_B = randrange(-200, 200) / 2
    x_attendu = x_B - x_A
    y_attendu = y_B - y_A
    assert vecteur(x_A, y_A, x_B, y_B) == (
        x_attendu,
        y_attendu,
    ), f"Erreur avec A {x_A, y_A} et B {x_B, y_B}"