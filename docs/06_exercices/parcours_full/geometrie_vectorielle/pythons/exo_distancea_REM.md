Il est aussi possible d'utiliser les fonctions précédentes :

```python
def distance(x_A, y_A, x_B, y_B):
    x_AB, y_AB = vecteur(x_A, y_A, x_B, y_B)
    return norme(x_AB, y_AB)
```
