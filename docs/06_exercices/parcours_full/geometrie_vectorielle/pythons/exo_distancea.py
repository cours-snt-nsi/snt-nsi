

# --------- PYODIDE:env --------- #

from math import sqrt

def norme(x_u, y_u):
    return sqrt(x_u**2 + y_u**2)


def vecteur(x_A, y_A, x_B, y_B):
    x_AB = x_B - x_A
    y_AB = y_B - y_A
    return (x_AB, y_AB)

# --------- PYODIDE:code --------- #

from math import sqrt


def distance(x_A, y_A, x_B, y_B):
    ...

# --------- PYODIDE:corr --------- #

from math import sqrt


def distance(x_A, y_A, x_B, y_B):
    return sqrt((x_B - x_A) ** 2 + (y_B - y_A) ** 2)

# --------- PYODIDE:tests --------- #

assert distance(0, 2, 3, 6) == 5.0
assert distance(10, 4, 5, -8) == 13.0

# --------- PYODIDE:secrets --------- #


# tests secrets
from random import randrange

for _ in range(10):
    x_A = randrange(-200, 200) / 2
    x_B = randrange(-200, 200) / 2
    y_A = randrange(-200, 200) / 2
    y_B = randrange(-200, 200) / 2
    attendu = sqrt((x_B - x_A) ** 2 + (y_B - y_A) ** 2)
    assert (
        abs(distance(x_A, y_A, x_B, y_B) - attendu) < 10**-6
    ), f"Erreur avec A {x_A, y_A} et B {x_B, y_B}"