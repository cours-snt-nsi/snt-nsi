# --- PYODIDE:code --- #
def puissance_inferieure(a, b):
    puissance = 1
    n = 0
    while ...:
        puissance = ...
        n = ...
    return ...


# --- PYODIDE:corr --- #
def puissance_inferieure(a, b):
    puissance = 1
    n = 0
    while puissance > b:
        puissance = puissance * a
        n = n + 1
    return n


# --------- PYODIDE:tests --------- #
assert puissance_inferieure(1/2, 1) == 0        # (1/2)^0 = 1
assert puissance_inferieure(1/2, 1/8) == 3      # (1/2)^3 = 1/8
assert puissance_inferieure(1/10, 0.0002) == 4  # (1/10)^3 = 0,001 et (1/10)^4 = 0,0001
# --------- PYODIDE:secrets --------- #
from math import log
from random import random

for _ in range(10):
    x = random()
    seuil = random()
    attendu = int(log(seuil, x)) + 1
    assert puissance_inferieure(x, seuil) == attendu, f"Erreur avec {x = } et {seuil = }"

