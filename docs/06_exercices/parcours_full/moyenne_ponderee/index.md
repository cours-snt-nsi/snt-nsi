---
author:
    - Sébastien Hoarau
hide:
    - navigation
    - toc
title: Moyenne pondérée(II)
tags:
    - à trous
    - dictionnaire
    - float
    - ep2
difficulty: 250
--- 

Un professeur de NSI décide de gérer les résultats de sa classe sous la forme d'un dictionnaire :

- les clés sont les noms des élèves ;
- les valeurs sont des dictionnaires dont les clés sont les types d'épreuves et les valeurs sont des listes dont le premier élément est la
 note obtenue, et le deuxième le coefficient associé.

Avec :

```python title=""
resultats_trimestre = {'Dupont':{'DS1': [15.5, 4],
                                 'DM1': [14.5, 1],
                                 'DS2': [13, 4],
                                 'PROJET1': [16, 3],
                                 'DS3': [14, 4]},
                       'Durand':{'DS1': [6 , 4],
                                 'DM1' : [14.5, 1],
                                 'DS2' : [8, 4],
                                 'PROJET1' : [9, 3],
                                 'IE1' : [7, 2],
                                 'DS3' : [8, 4],
                                 'DS4' :[15, 4]}}
```

L'élève dont le nom est Durand a ainsi obtenu au DS2 la note de $8$ avec un coefficient $4$.

L'élève dont le nom est Dupont était absent à l'épreuve DS4 : l'épreuve n'apparait donc pas dans son relevé de notes.

Le professeur crée une fonction `moyenne` qui prend en paramètres `nom` qui est le nom d'un de ces élèves et le dictionnaire `resultats` 
qui représente les résultats des ses élèves comme décrit ci-dessus. Cette fonction renvoie la moyenne de l'élève arrondie au dixième. 
Si le nom de l'élève n'apparait pas dans le dictionnaire des résultats, la fonction renvoie la valeur $-1$. 

!!! example "Exemples"


    ```pycon title=""
    >>> moyenne('Durand', resultats_trimestre)
    9.2
    >>> moyenne('Martin', resultats_trimestre)
    -1
    ```

Compléter le code du professeur ci-dessous :

{{ IDE('exo') }}

