Si on regarde les opérations couteuses, il s'agit uniquement des multiplications entre deux grands nombres. Il y en a trois à chaque fois qu'on fait un appel récursif.

On rappelle que le nombre de fois où on peut diviser un entier par deux est environ son logarithme en base 2.

Le cout de `fibonacci(n)` est donc environ $3\log(n)$ multiplications entre grands entiers. C'est nettement plus efficace qu'une méthode itérative classique qui fait $n$ additions entre grands entiers.

# :warning: Calcul modulaire

> Paragraphe pour les élèves en maths expertes ou en prépa, ou pour les enseignants

Souvent, on s'intéresse au résultat d'une opération modulo $m$ où $m$ est un entier qui tient dans un demi-mot machine ($m$ est aussi parfois un nombre premier). De sorte qu'un produit tient encore dans un conteneur de la taille d'un mot machine. On peut alors faire une réduction modulo $m$ et travailler uniquement modulo $m$.

Attention, ceci n'est valable que pour les opérations d'addition, soustraction et multiplication. Pour les divisions, c'est bien plus technique...

Voici alors une version de `fibonacci` qui prend un paramètre supplémentaire $m$ le modulo du résultat attendu.

!!! danger "Deux points techniques"
    Les plus avertis noteront deux choses :
    
    - il faut que $m$ soit légèrement plus petit que la moitié d'un mot machine. Plus précisément, ici, il faut que $3m^2$ tienne dans un mot machine.
    - $m$ est un entier non nul et 
        - pour $m>1$, on a `1 % m` qui est égal à `1`
        - mais dans le rare cas où $m=1$, on a `1 % m` égal à `0`
        - cet oubli est une source d'erreur délicate
        - Il ne faut donc pas écrire `#!py return 1` au lieu de `#!py return 1 % m`


```python
def fibonacci(n, m):
    """Renvoie (F_{n-1}, F_{n}) chacun modulo m
        deux nombres de Fibonacci consécutifs
    """
    # cas de base
    if n == 1:
        return (0, 1)
    
    # cas général
    u, v = fibonacci(n//2, m)
    u, v = (v*v + u*u)%m, (v*(v + 2*u))%m
    if n % 2 == 0:
        return (u, v)
    if n % 2 == 1:
        return (v, (u+v)%m)
```

Un tel code permet de calculer facilement les 9 derniers chiffres de $F_{10^{16}}$ alors qu'avec une méthode itérative classique, il faudrait plusieurs milliards d'années de calcul...

```pycon
>>> f_nm1, f_n = fibonacci(10**16, 10**9)
>>> f_n
560546875
```

Ainsi $F_{10^{16}} = \cdots 560546875$
