---
author:
    - Nicolas Revéret
    - Franck Chambon
difficulty: 370
hide:
    - navigation
    - toc
title: Plus longues sous-suites communes
tags:
    - programmation dynamique
    - récursivité
    - mémoïsation
    - à trous
maj: 01/03/2024
---


Considérons les deux « textes » `#!py "LAPIN"` et `#!py "CAPRIN"`.

À quel point sont-ils proches ?

Pour répondre à cette question, on peut se demander quelle est *la plus longue sous-suite commune* à ces deux textes.

???+ tip "Définition"

    On appelle **sous-suite** d'une chaîne de caractères, une chaîne produite en enlevant zéro, un ou plusieurs caractères.

    Par exemple, `#!py "AN"` est une sous-suite de `#!py "LAPIN"`.

    `#!py "AN'` est aussi une **sous-suite commune** à `#!py "LAPIN"` et `#!py "CAPRIN"`.

Dans certains cas, il peut y avoir plusieurs sous-suites communes de longueurs identiques. Par exemple les chaînes `#!py "abc"` et `#!py "bac"` admettent deux sous-suites communes de longueur 2 : `#!py "ac"` et `#!py "bc"`.

On va donc plutôt calculer la **longueur de la plus longue sous-suite commune** aux deux textes.

La comparaison de `#!py "LAPIN"` et `#!py "CAPRIN"` donne ceci (sur fond vert les caractères "identiques", sur fond rouge les "différents") :

!!! quote ""

    <p style="color:black;letter-spacing: 3px; font-family:consolas"><span style="background-color:#FF7276">L</span><span style="background-color:#90EE90">APIN</span></p>

    <p style="color:black;letter-spacing: 3px; font-family:consolas"><span style="background-color:#FF7276">C</span><span style="background-color:#90EE90">AP</span><span style="background-color:#FF7276">R</span><span style="background-color:#90EE90">IN</span></p>

La plus longue sous-suite commune est donc `#!py "APIN"`. Elle est de longueur 4.

Écrire la fonction `lplssc` (pour *longueur plus longue sous-suite commune*) qui prend en argument les deux textes à comparer (`texte_a` et `texte_b`) et renvoie la longueur de la plus longue sous-suite commune.

On propose deux versions de cette fonction : une version récursive utilisant la mémoïsation et une itérative.

???+ example "Exemples"

    ```pycon title=""
    >>> lplssc("LAPIN", "CAPRIN")
    4
    >>> lplssc("abcd", "abcde")
    4
    >>> lplssc("aBaBaBaB", "aaa")
    3
    ```


??? tip "Aide"

    Une approche consiste à déterminer les longueurs des plus longues sous-suites communes de toutes les versions de chaque texte :
    la version à `#!py 0` caractère, la version à `#!py 1` caractère, *etc*. Il y a `#!py (1 + len(texte_a))` × `#!py (1 + len(texte_b))` cas à envisager.

    En prenant encore une fois `#!py texte_a = "LAPIN"` et `#!py texte_b = "CAPRIN"`, les différents cas envisagés apparaissent dans le tableau ci-dessous. On trouve dans chaque case la longueur de la plus longue sous-suite commune à chaque texte s'il s'arrêtait au caractère indiqué en début de ligne ou de colonne.

    Ainsi, le `#!py 1` au bout de la double flèche est la longueur de la plus longue sous-suite commune aux textes `#!py "LA"` et `#!py "CAPRI"`.

    ![Tableau de longueurs](images/tableau.png){width=30% .center .autolight}

    On constate que :

    * la première colonne et la première ligne du tableau ne contiennent que des `#!py 0`. C'est en effet la longueur de la plus longue sous-suite commune dans le cas où l'un des textes est vide ;

    * si les derniers caractères considérés sont identiques, ce caractère commun fait partie de la sous-suite commune. On peut le supprimer et ajouter `#!py 1` au résultat de la case au dessus à gauche  (flèche en diagonale);

    * si les derniers caractères considérés diffèrent, l'un des deux caractères (ou les deux) ne fait pas partie de la sous-suite commune. On compare les valeurs des cases de gauche et du dessus (flèches horizontale et verticale).

??? question "Version récursive"

    Cette fonction fait appel à la fonction récursive `lplssc_rec` qui prend en paramètres le nombre de caractères de chaque texte à comparer `taille_a` et `taille_b`. Cette fonction renvoie la longueur de la plus longue sous-suite commune aux `taille_a` premiers caractères de `texte_a` et aux `taille_b` premiers caractères de `texte_b`.

    Par exemple, avec `#!py texte_a = "LAPIN"` et `#!py texte_b = "CAPRIN"`, l'appel `#!py lplssc_rec(2, 5)` renvoie la longueur de la plus longue sous-suite commune à `#!py "LA"` et `#!py "CAPRI"`.

    On gardera trace des résultats intermédiaires dans un dictionnaire `longueurs` qui, à chaque couple `(taille_a, taille_b)`, associe la longueur de la plus longue sous-suite commune.

    La fonction étant récursive, on débutera par l'appel prenant en compte tous les caractères de chaque texte.

    === "Version vide"
        {{ IDE('./pythons/exo_rec_a') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_rec_b') }}

??? question "Version itérative"

    Dans cette version itérative on détermine là encore les longueurs des plus longues sous-suites communes de toutes les versions de chaque texte.

    On peut toutefois remarquer que les deux formules de récurrences (derniers caractères identiques ou différents) ne font intervenir que deux lignes à la fois. On peut donc compléter le tableau ligne par ligne en partant de la première et en ne gardant trace que de la dernière ligne complétée.

    ![Une seule ligne est nécessaire](images/tableau_2.png){width=35% .center .autolight}

    === "Version vide"
        {{ IDE('./pythons/exo_iter_a') }}
    === "Version à compléter"
        {{ IDE('./pythons/exo_iter_b') }}
