---
author:
    - Nicolas Revéret
hide:
    - navigation
    - toc
title: Résolution d'une équation par balayage
tags:
    - en travaux
    - maths
difficulty: 110
maj: 06/06/2024
---

On cherche dans cet exercice à déterminer des valeurs approchées de solution d'équation par *balayage*. Cette démarche ne fournit pas nécessairement la valeur exacte mais elle est simple à mettre en œuvre.

On considère une fonction $f$ vérifiant les propriétés suivantes :

* $f$ est définie sur un intervalle $\left[a~;~b\right]$ ;
* $f$ est strictement monotone[^1] sur $\left[a~;~b\right]$ ;
* $f(a)$ et $f(b)$ sont de signes contraires ;
* $f$ est *continue*[^2] sur $\left[a~;~b\right]$.

[^1]: $f$ est donc soit strictement croissante soit strictement décroissante sur $\left[a~;~b\right]$.
[^2]: la continuité est une propriété mathématique qui assure que le graphe de $f$ peut être tracé sans lever le crayon.

Ces propriétés garantissent que la fonction $f$ s'annule une unique fois sur $\left[a~;~b\right]$. On cherche donc à résoudre l'équation $f(x)=0$. On appelle $\alpha$ l'unique solution de cette équation.

![Résolution par balayage](graphe_1.svg){ .autolight .center width=45%}

Afin de déterminer un encadrement de largeur $h$ de $\alpha$, on procède comme suit :

* on considère une variable $x$ initialement égale à $a$ ;
* tant que $f(x + h)$ est du signe de $f(a)$, on ajoute $h$ à $x$. On balaie donc l'intervalle de $h$ en $h$ ;
* dès que $f(x + h)$ change de signe, on renvoie le couple $(x~;~x+h)$ : c'est notre encadrement.

On présente ci-dessous deux versions de l'exercice : l'une pour déterminer un encadrement de $\sqrt2$, l'autre plus générale.


??? question "Encadrement de $\sqrt2$"

    La fonction considérée est $f:x\mapsto x^2-2$.
    
    On sait que $f(1)=1^2-2=-1<0$ et $f(2)=2^2-2=2>0$. De plus cette fonction est continue sur $\left[1~;~2\right]$.
    
    L'équation $f(x)=0$ admet donc une unique solution sur $\left[1~;~2\right]$. Cette solution est $\sqrt2$.
    
    Comme $f(1)$ est strictement négatif, on balaie l'intervalle jusqu'à ce que $f(x+h)$ soit strictement positif.
    
    Compléter la fonction `balayage` qui prend en paramètres deux nombres `a` et `h` et renvoie un encadrement de $\sqrt2$ de largeur `h`.
    
    Comme son nom l'indique, la variable `racine_de_2` contient une valeur approchée de $\sqrt2$.
    
    Les tests de cet exercice vérifient que `racine_de_2` est bien comprise entre les deux valeurs renvoyées et que la largeur entre celles-ci est bien « *égale*[^1] » à `h`.
    
    [^1]: on ne teste pas l'égalité stricte de l'écart en raison de possibles erreurs d'arrondis.
    
    ???+ example "Exemples"

        ```pycon title=""
        >>> balayage(1, 0.1)
        (1.4000000000000004, 1.5000000000000004)
        >>> balayage(1.4, 0.01)
        (1.41, 1.42)
        >>> balayage(1.41, 0.001)
        (1.4139999999999995, 1.4149999999999994)
        ```
    
    === "Version vide"
        {{ IDE('exo_racine_2_vide') }}
    === "Version à compléter"
        {{ IDE('exo_racine_2_trous') }}


??? question "Cas général"
    
    On considère désormais une fonction $f$ quelconque qui sera passée en paramètre de la fonction `balayage`.
    
    On sait que $f(a)$ et $f(b)$ sont de signes contraires mais on ignore si $f(a)$ est positif ou négatif... Or la méthode par balayage nécessite de savoir si $f(x+h)$ et $f(a)$ ont le même signe ou non !
    
    Cette condition peut être testée astucieusement : $f(x + h)$ est du signe de $f(a)$ si leur produit $f(x + h) \times f(a)$ est positif ou nul (en effet, le produit de deux nombres de mêmes signes est positif).

    Compléter la fonction `balayage` qui prend en paramètres la fonction $f$ étudiée et deux nombres `a` et `h` et renvoie un encadrement de la solution de $f(x)=0$ de largeur `h`.
    
    Les tests de cet exercice vérifient que la solution attendue est bien comprise entre les deux valeurs renvoyées et que la largeur entre celles-ci est bien « *égale* » à `h`.
    
    ???+ example "Exemples"

        La fonction `f` renvoie `#!py x*x -3`. La valeur cherchée dans cet exemple est donc $\sqrt3$.
        
        Attention, les tests proposent d'autres fonctions `f`.

        ```pycon title=""
        >>> balayage(f, 0, 1)
        (1, 2)
        >>> balayage(f, 1, 0.1)
        (1.7000000000000006, 1.8000000000000007)
        >>> balayage(f, 1.7, 0.01)
        (1.73, 1.74)
        ```
        
    === "Version vide"
        {{ IDE('exo_f_vide') }}
    === "Version à compléter"
        {{ IDE('exo_f_trous') }}

