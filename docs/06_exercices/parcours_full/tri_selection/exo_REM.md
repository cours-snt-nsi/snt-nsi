On peut écrire le code en un seul script.
De plus, il suffit, lors du parcours de la liste à trier, de s'arrêter à l'avant dernier élément de cette liste.

```python title=""
def tri_selection(tableau):
    for i in range(len(tableau) - 1):
        i_mini = i
        for j in range(i + 1, len(tableau)):
            if tableau[j] < tableau[i_mini]:
                i_mini = j
        tableau[i], tableau[i_mini] = tableau[i_mini], tableau[i]
```