---
author: Nicolas Revéret
hide:
    - navigation
    - toc
title: Train en POO
tags:
    - à trous
    - programmation orientée objet
difficulty: 270
---

# La classe Train

On souhaite dans cet exercice créer une classe `Train` permettant de relier des objets de type `Wagon`.

Un objet de type `Wagon` possède deux attributs :

* un contenu `contenu` de type `str`,
* un lien vers le wagon suivant `suivant` de type `Wagon`.

On inclut aussi deux méthodes permettant d'afficher le wagon dans la console ou sous forme d'une chaine de caractère.

??? info "La classe `Wagon` (pour information)"

    ```python
    class Wagon:
        def __init__(self, contenu):
            "Constructeur"
            self.contenu = contenu
            self.suivant = None

        def __repr__(self):
            "Affichage dans la console"
            return f'Wagon de {self.contenu}'

        def __str__(self):
            "Conversion en string"
            return self.__repr__()
    ```

Cette classe est déjà "importée" dans la zone de saisie ci-dessous. **Il est inutile de la recopier**.

Un objet de la classe `Train` possède deux attributs :

* `premier` contient son premier wagon (de type `Wagon`) ou `None` si le train est vide (il n'y a que la locomotive),
* `nb_wagons` (de type `int`) contient le nombre de wagons attachés à la locomotive.

Lors de sa création, un objet de type `Train` sera toujours vide.

Les méthodes de la classe `Train` sont présentées ci-dessous (`train` est un objet de type `Train`) :

*  `train.est_vide()` renvoie `True` si `train` est vide (ne comporte aucun wagon), `False` sinon ;
*  `train.donne_nb_wagons()` renvoie le nombre de wagons de `train` ;
*  `train.transporte_du(contenu)` détermine si `train` transporte du `contenu` (une chaine de caractères). Renvoie `True` si c'est le cas, `False` sinon ;
*  `train.ajoute_wagon(wagon)` ajoute un `wagon` à la fin du train. On passe en argument le `wagon` à ajouter ;
*  `train.supprime_wagon_de(contenu)` prend en argument une chaine de caractères `contenu` et supprime le premier wagon de `contenu` du train. Si le `train` est vide ou ne comporte aucun wagon de `contenu`, la méthode renvoie `False`. S'il en contient un et que celui-ci est effectivement supprimé, la méthode renvoie `True`.

On inclut là-aussi aussi deux méthodes permettant d'afficher le train dans la console ou sous forme d'une chaine de caractères. 

???+ example "Exemples"

    * Création d'un train vide :
  
    ```pycon
    >>> train = Train()
    ```

    * Ajout de wagons :
  
    ```pycon
    >>> w1 = Wagon('blé')
    >>> train.ajoute_wagon(w1)
    >>> w2 = Wagon('riz')
    >>> train.ajoute_wagon(w2)
    >>> train.ajoute_wagon(Wagon('sable'))
    >>> train
    'Locomotive - Wagon de blé - Wagon de riz - Wagon de sable'
    ```

    * Description du train :
  
    ```pycon
    >>> train.est_vide()
    False
    >>> train.donne_nb_wagons()
    3
    >>> train.transporte_du('blé')
    True
    >>> train.transporte_du('matériel')
    False
    ```

    * Suppression de wagon

    ```pycon
    >>> train.supprime_wagon_de('riz')
    True
    >>> train
    'Locomotive - Wagon de blé - Wagon de sable'
    >>> train.supprime_wagon_de('riz')
    False
    ```

On pourra parcourir tous les wagons du train en utilisant les instructions ci-dessous :

```python
wagon = self.premier
while wagon is not None:
    # actions à effectuer
    wagon = wagon.suivant
```

En plusieurs occasions il faudra prendre soin de traiter séparément le cas du premier wagon et celui des suivants.

Enfin, lors de la suppression d'un wagon, on se contentera de l'omettre en liant son wagon précédent à son suivant. La figure ci-dessous illustre ainsi l'instruction `train.supprime_wagon_de('riz')` avant et après la suppression.

![Suppression](suppression.svg){ width="75%" }

{{ IDE('exo') }}
