

# --------- PYODIDE:code --------- #

def transformation(motif, regles):
    ...


def n_transformations(motif, regles, n):
    ...

# --------- PYODIDE:corr --------- #

def transformation(motif, regles):
    resultat = ""
    for caractere in motif:
        if caractere in regles:
            resultat += regles[caractere]
        else:
            resultat += caractere
    return resultat


def n_transformations(motif, regles, n):
    for _ in range(n):
        motif = transformation(motif, regles)
    return motif

# --------- PYODIDE:tests --------- #

regles = {"a": "ab", "b": "ac", "c": "d"}
motif = "a"
assert transformation(motif, regles) == "ab"
assert n_transformations(motif, regles, 2) == "abac"
assert n_transformations(motif, regles, 3) == "abacabd"
motif = "abc"
assert transformation(motif, regles) == "abacd"
assert n_transformations(motif, regles, 3) == "abacabdabacdd"
motif = "ddddd"
assert n_transformations("ddddd", regles, 50) == "ddddd"

# --------- PYODIDE:secrets --------- #


# Tests supplémentaires
# Identité
regles = {"a": "a", "b": "b"}
motif = "ab"
assert transformation(motif, regles) == "ab"
assert n_transformations(motif, regles, 20) == "ab"

# Doublement
regles = {"a": "aa"}
motif = "a"
assert transformation(motif, regles) == "aa"
assert n_transformations(motif, regles, 4) == "aaaaaaaaaaaaaaaa"

# On échange
regles = {"a": "b", "b": "a"}
motif = "ac"
assert transformation(motif, regles) == "bc"
assert n_transformations(motif, regles, 4) == "ac"
assert n_transformations(motif, regles, 5) == "bc"

# Pas de transformation -> cas limite
assert n_transformations(motif, regles, 0) == motif