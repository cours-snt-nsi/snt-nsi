---
hide:
    - toc
---

# 🚶 Parcours

On propose ici différents parcours permettant d'explorer un algorithme, une thématique ou une structure de données...

??? abstract "Algorithmes gloutons"

    On utilise des méthodes gloutonnes.

    - [Premier minimum local ](./parcours_full/premier_minimum/index.md) : déterminer l'indice du premier minimum dans un tableau


??? abstract "Calcul de moyennes"

    On calcule ici les moyennes dans un tableau, ou dans des structures plus complexes.

    - [Moyenne simple ](./parcours_full/moyenne/index.md) : calculer une moyenne
    - [Moyenne olympique ](./parcours_full/moy_olympic/index.md) : calculer une moyenne en excluant la valeur maximale et la valeur minimale
    - [Moyenne pondérée ](./parcours_full/moyenne_ponderee/index.md) : calculer une moyenne pondérée

??? abstract "Dictionnaires : construction"

    Des exercices demandant de construire des dictionnaires.

    - [Valeurs extrêmes ](./parcours_full/dict_extremes/index.md) : extraire le dictionnaire `{"mini": m, "maxi": M}` d'un tableau
    - [Dictionnaire d'occurrences ](./parcours_full/dico_occurrences/index.md) : compter les occurrences des caractères dans une chaine
    - [Dictionnaire des antécédents ](./parcours_full/antecedents/index.md) : déterminer le dictionnaire « réciproque » d'un dictionnaire : `{valeur: [clés associées]}`


??? abstract "Dictionnaires: utilisation"

    On y parcourt des dictionnaires.

    - [Anniversaires ](./parcours_full/anniversaires/index.md) : déterminer les clés dont les valeurs associées vérifient une certaine condition
    - [Couleurs ](./parcours_full/couleurs/index.md) : convertir la représentation d'une couleur en hexadécimal à du RGB
    - [L-système ](./parcours_full/lsystem/index.md) : « calculer » une nouvelle chaine de caractères en respectant les règles contenues dans un dictionnaire
    - [Top-like ](./parcours_full/top_like/index.md) : déterminer la clé de valeur maximale

??? abstract "Graphes"

    <center>
    ```mermaid
    flowchart LR
    G ---|5| R
    G ---|2| A
    R ---|3| A
    A --- |1| P
    P --- |6| H
    P --- |6| E
    H --- |1| E
    ```
    </center>
    
    - [La rumeur qui court ](./parcours_full/rumeur/index.md) : parcours en largeur
    - [Degré de séparation ](./parcours_full/degres_separation/index.md) : illustration des petits mondes
    - [Aimer et être aimé ](./parcours_full/aimer_etre_aime/index.md) : renverser un graphe orienté
    - [Cadeaux circulaires ](./parcours_full/permutation_circulaire/index.md) : un graphe est-il cyclique ?
    - [Liste et matrice d'adjacence ](./parcours_full/liste_adjacence/index.md) : passer d'une représentation d'un graphe à l'autre
    - [Matrice d'adjacence ](./parcours_full/matrice_adjacence/index.md) : deux exercices autour des matrices d'adjacence
    - [Mme Tortue et M. Lièvre ](./parcours_full/lievre_tortue/index.md) : algorithme de Floyd

??? abstract "Manipulation de chaines de caractères"

    Tout est dans le titre !

    - [Dentiste ](./parcours_full/dentiste/index.md) : supprimer les voyelles d'une chaine de caractères
    - [Mots qui se correspondent ](./parcours_full/correspond_mot/index.md) : comparer deux chaines de caractères
    - [Renverser une chaine ](./parcours_full/renverse_chaine/index.md) : comme son nom l'indique !
    - [Collage ](./parcours_full/join/index.md) : former une chaine à partir des éléments d'une liste... réécrire `" ".join(mots)` !
    - [Découpe ](./parcours_full/split/index.md) : découper une chaine à chaque espace... réécrire `chaine.split(' ')` !
    - [Code de César ](./parcours_full/code_cesar/index.md) : chiffrer une chaine de caractère à l'aide du code de César
    - [Texte inclus ](./parcours_full/est_inclus/index.md) : recherche d'un motif dans une chaine de caractères
    - [Texte brut ](./parcours_full/brut/index.md) : extraire le contenu textuel d'une source `HTML`

??? abstract "Maths : Seconde"

    - [Fonctions affines ](./parcours_full/fonctions_affines/index.md) : plusieurs exercices autour des fonctions affines
    - [Fonctions du 2nd degré ](./parcours_full/second_degre_1/index.md) : plusieurs exercices autour des fonctions polynômes du seconde degré
    - [Géométrie vectorielle ](./parcours_full/geometrie_vectorielle/index.md) : plusieurs exercices de géométrie vectorielle
    - [Résolution d'équation par balayage ](./parcours_full/balayage/index.md) : résolution approchée d'une équation
    - [Moyenne simple ](./parcours_full/moyenne/index.md) : calculer une moyenne
    - [Moyennes pondérée ](./parcours_full/moy_ponderee/index.md) : calculer une moyenne pondérée
    - [Puissances d'un nombre ](./parcours_full/seuil_puissances/index.md) : déterminer la première puissance d'un nombre dépassant ou passant sous un seuil
    - [Multiple ](./parcours_full/multiple/index.md) : déterminer si un entier est un multiple d'un autre
    - [Diviseurs d'un entier ](./parcours_full/diviseurs/index.md) : déterminer les diviseurs d'un entier positif
    - [Divisibilité par 3 ](./parcours_full/divisible_par_3/index.md) : trois méthodes pour étudier la divisibilité par trois
    - [Nombre Premier ](./parcours_full/nombre_premier/index.md) : déterminer si un (petit) nombre entier est premier

??? abstract "Parcours de tableaux"

    Ici l'on filtre des tableaux, on vérifie qu'un tableau est trié.

    - [Écrêtage ](./parcours_full/ecretage/index.md) : remplacer les valeurs extrêmes d'un tableau
    - [Soleil couchant ](./parcours_full/soleil_couchant/index.md) : déterminer tous les *maxima* relatifs d'un tableau
    - [Nombres puis double ](./parcours_full/nb_puis_double/index.md) : rechercher des couples de valeurs consécutives particulières dans un tableau
    - [Est trié ? ](./parcours_full/est_trie/index.md) : le tableau est-il trié ?
    - [Liste des différences ](./parcours_full/liste_differences/index.md) : comparer deux tableaux
    - [Gelées ](./parcours_full/gelee/index.md) : longueur d'un sous-tableau particulier
    - [Tous différents ](./parcours_full/tous_differents/index.md) : les éléments d'un tableau sont-ils tous différents ?


??? abstract "Programmation dynamique"

    - [Communication des acacias ](./parcours_full/acacias/index.md) : somme maximale dans un tableau sous contrainte
    - [Plus longues sous-chaines ](./parcours_full/lplssc/index.md) : plus longues sous-chaines communes à deux chaines

??? abstract "Programmation orientée objet"

    Il s'agit ici d'utiliser des classes proposées ou de les écrire.

    - [Chien en POO ](./parcours_full/poo_chien/index.md) : compléter une classe représentant un chien
    - [Train en POO ](./parcours_full/poo_train/index.md) : compléter une classe représentant un train
    - [Hauteur et taille d'un arbre binaire ](./parcours_full/arbre_bin/index.md) : compléter les méthodes `hauteur` et `taille`
    - [Arbre binaire de recherche ](./parcours_full/ABR/index.md) : compléter les méthodes `insere`, `est_present`, `affichage_infixe`

??? abstract "Recherche dans un tableau"

    On recherche ici les _extrema_ dans un tableau (minimum ou maximum), une valeur ou un indice particulier.

    - [Maximum ](./parcours_full/maximum_nombres/index.md) : déterminer le maximum dans un tableau
    - [Indice du minimum ](./parcours_full/ind_min/index.md) : déterminer l'indice du minimum dans un tableau
    - [Premier minimum local ](./parcours_full/premier_minimum/index.md) : déterminer l'indice du premier minimum dans un tableau
    - [Occurrences du minimum ](./parcours_full/occurrences_du_mini/index.md) : déterminer la valeur et les indices du minimum
    - [Valeur et indices du max ](./parcours_full/val_ind_max/index.md) : déterminer la valeur et les indices du maximum

??? abstract "Récursivité"

    Voir « Récursivité »

    - [Suite de Fibonacci (1) ](./parcours_full/fib_1/index.md) : une fonction très élémentaire, pour des indices inférieurs à 25
    - [Anagrammes ](./parcours_full/anagrammes/index.md) : deux chaines sont-elles des anagrammes ?
    - [Percolation ](./parcours_full/percolation/index.md) : écoulement d'eau dans un sol, ou plutôt parcours en profondeur dans une grille
    - [Pavage possible avec triominos (2) ](./parcours_full/pavage_triomino_2/index.md) : Construire un pavage, si possible, d'un carré troué avec des triominos
    - [Énumération des permutations ](./parcours_full/nb_factoriel/index.md) : les nombres factoriels
    - [Énumération des chemins à Manhattan ](./parcours_full/nb_chemins_grille/index.md) : relier deux points en ne se déplaçant que vers la droite ou vers le haut
    - [Énumération des chemins de Schröder ](./parcours_full/nb_schroder/index.md) : mouvements ↗ , →→, ou ↘
    - [Énumération des chemins de Delannoy ](./parcours_full/nb_delannoy/index.md) : mouvements ↗, →, ou ↘
    - [Énumération des arbres binaires de taille n ](./parcours_full/nb_catalan_2/index.md) : les nombres de Catalan
    - [Énumération des arbres unaires-binaires à n+1 nœuds ](./parcours_full/nb_motzkin/index.md) : les nombres de Motzkin
    - [Énumération des subdivisions de polygone ](./parcours_full/nb_hipparque/index.md) : Les nombres d'Hipparque
    - [Suite de Fibonacci (2) ](./parcours_full/fib_2/index.md) :boom: :boom: : Calcul possible du millionième terme
    - [Nombre de partitions d'un entier ](./parcours_full/nb_partitions/index.md)

??? abstract "Structures conditionnelles"

    Si c'est vrai il faut faire ceci, sinon cela...

    - [Autour des booléens ](./parcours_full/autour_des_booleens/index.md) : écrire des tests tels que « $15$ est-il dans la table de $3$ et de $5$ ? »
    - [Années bissextiles ](./parcours_full/bissextile/index.md) : déterminer si une année est bissextile ou non
    - [Opérateurs booléens ](./parcours_full/operateurs_booleens/index.md) : écrire les opérateurs booléens sans utiliser les opérateurs booléens !
    - [Multiplier sans * ](./parcours_full/multiplication/index.md) : multiplier deux entiers relatifs sans utiliser le symbole `*`
    - [Suite de Syracuse ](./parcours_full/syracuse/index.md) : la fameuse suite $3n+1$
    - [Tous différents ](./parcours_full/tous_differents/index.md) : les éléments d'un tableau sont-ils tous distincts (solution en temps quadratique)
    - [Noircir un texte ](./parcours_full/anonymat_1/ondex.md") }} » et [Caviarder un texte ](./parcours_full/anonymat_2/index.md) : effacer les caractères alphabétiques dans un texte

??? abstract "Structures de données"

    On y utilise ou met en œuvre des listes chaînées, piles, files, arbres...

    * [Train en POO ](./parcours_full/poo_train/index.md) : une liste chainée en réalité
    * [Bien parenthésée 2 ](./parcours_full/expression_bien_parenthesee_2/index.md) : une expression est-elle bien parenthésée ?
    * [Hauteur et taille d'un arbre ](./parcours_full/arbre_enracine/index.md) : on y représente les arbres sous forme de liste de listes Python
    * [Hauteur et taille d'un arbre binaire ](./parcours_full/arbre_bin/index.md) : on y représente les arbres à l'aide de la programmation orientée objet

??? abstract "Tris"

    Les classiques !

    - [Tri par sélection ](./parcours_full/tri_selection/index.md)
    - [Tri à bulles ](./parcours_full/tri_bulles/index.md)
    - [Insertion dans une liste triée ](./parcours_full/insertion_liste_triee/index.md)
    - [Fusion de listes triées ](./parcours_full/fusion_listes_triees/index.md)
    - [Tas min ](./parcours_full/tas_min/index.md) : vérifier qu'un tableau représente un tas minimal

