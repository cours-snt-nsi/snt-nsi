---
author: Nicolas Revéret
hide:
    - navigation
    - toc
reviews: Franck Chambon, Sébastien Hoarau
title: Hauteur et taille d'un arbre
---

# Taille et hauteur d'un arbre

Tous les arbres étudiés dans cet exercice sont **non-vides**.

On représente dans cet exercice les arbres enracinés à l'aide de listes Python dont le premier élément est la valeur de la racine et le second la liste de ses enfants.

Par exemple l'arbre :

```text title="Un arbre"
        A
       /|\
      / | \
     B  C  D
     |
     E
```

est représenté, ici, avec Python, par `["A", [["B", [["E", []]]], ["C", []], ["D", []]]]`.


## Objectif

On demande d'écrire les fonctions `taille` et `hauteur` prenant en paramètre un `arbre` répondant à cette représentation et renvoyant :

* pour la fonction `taille` le nombre de nœuds de l'arbre ;

* pour la fonction `hauteur` la hauteur de l'arbre (longueur, en nombre d'arêtes, du plus long chemin entre la racine et une feuille). Un arbre réduit à sa racine a donc une hauteur de 0.

## Exemples

```pycon
>>> taille(["A", [["B", []]]])
2
>>> hauteur(["A", [["B", []]]])
1
>>> taille(["A", [["B", []], ["C", []]]])
3
>>> hauteur(["A", [["B", []], ["C", []]]])
1
```
