---
author: Nicolas Revéret
hide:
    - navigation
    - toc
reviews: Franck Chambon, Sébastien Hoarau
title: Réduire une liste
---

# Réduire une liste

On considère dans cet exercice des listes **non vides** d'entiers.

## Objectif

On demande d'écrire deux fonctions :

* `consecutifs` prend en paramètre une telle liste et renvoie la liste contenant le nombre de valeurs consécutives identiques lues,

* `reduction` prend en paramètre une telle liste et répète l'opération précédente jusqu'à ce que la liste soit de longueur 1. On renvoie alors l'unique valeur de la liste.

Ainsi, l'appel `consecutifs([8, 5, 5, 2, 2, 2])` renvoie `[1, 2, 3]`, car la liste initiale contient un `#!py 8` suivi de deux `#!py 5` puis de trois `#!py 2`.

Le nouvel appel `consecutifs([1, 2, 3])` renvoie `[1, 1, 1]`.

Un autre appel `consecutifs([1, 1, 1])` renvoie `[3]` qui est de longueur 1.

Ainsi l'appel `reduction([8, 5, 5, 2, 2, 2])` renvoie directement `#!py 3`.

## Exemples

```pycon
>>> consecutifs([8, 5, 5, 2, 2, 2])
[1, 2, 3]
>>> consecutifs([1, 2, 3])
[1, 1, 1]
>>> consecutifs([1, 1, 1])
[3]
```

```pycon
>>> reduction([8, 5, 5, 2, 2, 2])
3
```

```pycon
>>> consecutifs([-3, -3, 5, 5, 6, -7])
[2, 2, 1, 1]
>>> reduction([-3, -3, 5, 5, 6, -7])
2
```
