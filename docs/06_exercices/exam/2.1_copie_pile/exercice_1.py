class Pile:
    def __init__(self):
        self.valeurs = []

    def est_vide(self):
        """Détermine si la pile est vide"""
        return len(self.valeurs) == 0

    def empile(self, x):
        """Empile x dans la pile"""
        self.valeurs.append(x)

    def depile(self):
        """Dépile une valeur et la renvoie
        Lève une erreur si la pile est vide
        """
        if self.est_vide():
            raise ValueError("La pile est vide")
        return self.valeurs.pop()

    def __eq__(self, autre):
        """Détermine si pile_1 == pile_2"""
        return self.valeurs == autre.valeurs


def copie(pile):
    pass
