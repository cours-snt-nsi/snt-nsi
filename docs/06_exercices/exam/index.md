---
author: Nicolas Revéret
reviews: Franck Chambon, Sébastien Hoarau
title: 🐦 Examen
hide:
    - toc
---

# Conditions d'examen

Les sujets proposés ci-dessous sont des sujets originaux

- sans lien avec les sujets de l'épreuve officielle,
- présentés dans les conditions de l'examen,
- sans solution disponible.

!!! warning "Niveau des exercices"

    Bien que parfaitement conformes au programme de la spécialité NSI en vigueur, certains des exercices proposés ci-dessous sont d'un niveau supérieur à ceux que l'on retrouve dans la Banque Nationale de Sujets.

    Leur finalité est l'entraînement en classe.

Chaque sujet propose deux exercices :

- le premier reprend un algorithme, un type ou une structure de données classique,
- le second est plus original. Il est accompagné d'un code à trous.

Vous trouverez donc dans chaque sujet quatre fichiers en téléchargement :

- les versions `pdf` de chaque exercice,
- le fichier `exercice_1.py` qui ne contient que la signature de la fonction à écrire,
- le fichier `exercice_2.py` qui contient le code à trous.

Dans les deux fichiers **Python** proposés, **les tests ne sont inclus**.

L'archive suivante contient l'ensemble des fichiers : [`sujets.zip`](sujets.zip)

??? question "Sujet 1"

    * [Exercice 1 :](1.1_qui_aime/index.md) *Qui aime ?*

        Créer une chaine de caractères ([pdf](1.1_qui_aime/sujet.pdf), [python](1.1_qui_aime/exercice_1.py))

    * [Exercice 2 :](1.2_AB_equilibre/index.md) *Arbre binaire équilibré*

        Comparaison des hauteurs des sous-arbres d'un arbre binaire ([pdf](1.2_AB_equilibre/sujet.pdf), [python](1.2_AB_equilibre/exercice_2.py))

??? question "Sujet 2"

    * [Exercice 1 :](2.1_copie_pile/index.md) *Copier une pile*

        Copier une pile dans le cadre de la Programmation Orientée Objet ([pdf](2.1_copie_pile/sujet.pdf), [python](2.1_copie_pile/exercice_1.py))

    * [Exercice 2 :](2.2_portions/index.md) *Nombre de portions*

        Parcours dans deux dictionnaires ([pdf](2.2_portions/sujet.pdf), [python](2.2_portions/exercice_2.py))

??? question "Sujet 3"

    * [Exercice 1 :](3.1_hauteur_taille/index.md) *Hauteur et taille d'un arbre*

        Hauteur et taille d'un arbre enraciné ([pdf](3.1_hauteur_taille/sujet.pdf), [python`](3.1_hauteur_taille/exercice_1.py))

    * [Exercice 2 :](3.2_serpent_chameau/index.md) *Serpent en chameau*

        Transformation d'une chaine de caractères ([pdf](3.2_serpent_chameau/sujet.pdf), [python](3.2_serpent_chameau/exercice_2.py))

??? question "Sujet 4"

    * [Exercice 1 :](4.1_point_minimal/index.md) *Point minimal*

        Recherche du `tuple` minimal dans une liste ([pdf](4.1_point_minimal/sujet.pdf), [python](4.1_point_minimal/exercice_1.py))

    * [Exercice 2 :](4.2_filtre_pile/index.md) *Filtrer une pile*

        Filtrer les nombres positifs et les négatifs dans une pile ([pdf](4.2_filtre_pile/sujet.pdf), [python](4.2_filtre_pile/exercice_2.py))

??? question "Sujet 5"

    * [Exercice 1 :](5.1_raccourcir_chaine/index.md) *Raccourcir une chaine*

        Transformation de chaines de caractères selon leur longueur ([pdf](5.1_raccourcir_chaine/sujet.pdf), [python](5.1_raccourcir_chaine/exercice_1.py))

    * [Exercice 2 :](5.2_residu/index.md) *Résidu d'un entier*

        Somme itérée des chiffres d'un entier ([pdf](5.2_residu/sujet.pdf), [python](5.2_residu/exercice_2.py))

??? question "Sujet 6"

    * [Exercice 1 :](6.1_parcours_largeur/index.md) *Parcours en largeur*

        Parcours en largeur à l'aide d'une file ([pdf](6.1_parcours_largeur/sujet.pdf), [python](6.1_parcours_largeur/exercice_1.py))

    * [Exercice 2 :](6.2_reduire/index.md) *Réduire une liste*

        Compter les répétitions dans une liste ([pdf](6.2_reduire/sujet.pdf), [python](6.2_reduire/exercice_2.py))

??? question "Sujet 7"

    * [Exercice 1 :](7.1_score_maxi/index.md) *Score maximal*

        Manipulation des indices et valeurs d'un tableau, recherche du maximum ([pdf](7.1_score_maxi/sujet.pdf), [python](7.1_score_maxi/exercice_1.py))

    * [Exercice 2 :](7.2_chameau_serpent/index.md) *Chameau en serpent*

        Transformation d'une chaine de caractères ([pdf](7.2_chameau_serpent/sujet.pdf), [python](7.2_chameau_serpent/exercice_2.py))

??? question "Sujet 8"

    * [Exercice 1 :](8.1_suppression_copies/index.md) *Suppression de copies*

        Supprimer les doublons consécutifs dans une liste ([pdf](8.1_suppression_copies/sujet.pdf), [python](8.1_suppression_copies/exercice_1.py))

    * [Exercice 2 :](8.2_voisin_maximal/index.md) *Voisin maximal*

        Valeur maximale dans une grille ([pdf](8.2_voisin_maximal/sujet.pdf), [python](8.2_voisin_maximal/exercice_2.py))

??? question "Sujet 9"

    * [Exercice 1 :](9.1_tri_selection/index.md) *Tri par sélection*

        Tri par sélection ([pdf](9.1_tri_selection/sujet.pdf), [python](9.1_tri_selection/exercice_1.py))

    * [Exercice 2 :](9.2_parenthesage/index.md) *Parenthésage correct*

        Cas où la chaîne comporte plusieurs types de délimiteurs ([pdf](9.2_parenthesage/sujet.pdf), [python](9.2_parenthesage/exercice_2.py))

??? question "Sujet 10"

    * [Exercice 1 :](10.1_tri_insertion/index.md) *Tri par insertion*

        Tri par insertion ([pdf](10.1_tri_insertion/sujet.pdf), [python](10.1_tri_insertion/exercice_1.py))

    * [Exercice 2 :](10.2_unique/index.md) *Unique*

        Recherche de l'élément n'apparaissant qu'une seule fois dans un tableau ([pdf](10.2_unique/sujet.pdf), [python](10.2_unique/exercice_2.py))
