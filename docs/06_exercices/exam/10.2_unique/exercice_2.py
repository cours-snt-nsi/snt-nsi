def unique(valeurs):
    occurrences = dict()
    for element in valeurs:
        if element in occurrences:
            ...
        else:
            ...

    for ... in ...:
        if ...:
            return ...
