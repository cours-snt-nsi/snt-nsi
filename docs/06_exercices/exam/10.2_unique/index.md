---
author: Nicolas Revéret
hide:
    - navigation
    - toc
reviews: Franck Chambon, Sébastien Hoarau
title: Valeur unique
---

# Valeur unique

On se donne une liste non vide contenant des nombres entiers.

Tous ces nombres apparaissent plusieurs fois dans la liste sauf un qui est unique.

## Objectif

On demande d'écrire la fonction `unique` prenant en paramètre cette liste et renvoyant la valeur n'apparaissant qu'une seule fois.

On garantit que la liste contient au minimum trois éléments (deux identiques et un unique).

On veillera à proposer un algorithme efficace. Dans ce cadre, utiliser les fonctions de tri proposées par Python n'est pas efficace. Il pourra par contre être intéressant d'utiliser un dictionnaire afin de compter les occurrences de chaque valeur.

## Exemples

```pycon
>>> unique([1, 2, 1, 2, 2, 1, 3, 1, 1, 2, 1])
3
>>> unique([-13, -13, 13, -13, -13])
13
>>> unique([0, 2, 0])
2
```
