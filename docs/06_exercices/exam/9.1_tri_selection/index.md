---
author: Nicolas Revéret
hide:
    - navigation
    - toc
reviews: Franck Chambon, Sébastien Hoarau
title: Tri par sélection
---

# Tri par sélection

On souhaite dans cet exercice coder le tri par sélection en Python.

On rappelle la démarche utilisée par ce tri :

* une boucle principale parcourt les indices entre le premier et l'avant-dernier (inclus l'un et l'autre),

* une boucle secondaire recherche l'indice de la valeur minimale située à droite de l'indice courant (inclus),

* cet indice minimal étant trouvé, on échange l'élément minimal avec l'élément d'indice courant.

## Objectif

On demande d'écrire deux fonctions :

* `i_mini_depuis` prend en paramètres un `tableau` et un indice `i` et renvoie l'indice de l'élément de `tableau` de valeur minimale et d'indice supérieur ou égal à `i`,

* `tri_selection` prend en paramètre un `tableau` et le trie en appliquant le tri par sélection.

On garantit que :

* `tableau` n'est pas vide,

* l'indice `i` passé à la fonction `i_mini_depuis` est un indice valide (compris entre `#!py 0` et `len(tableau) - 1` inclus).

## Exemples

```pycon
>>> tableau = [3, 1, 2]
>>> tri_selection(tableau)
>>> tableau
[1, 2, 3]
>>> tableau = [10, -1, 3, 7, -2, 0, 2]
>>> tri_selection(tableau)
>>> tableau
[-2, -1, 0, 2, 3, 7, 10]
```
