---
author: Nicolas Revéret
hide:
    - navigation
    - toc
reviews: Franck Chambon, Sébastien Hoarau
title: Parenthésage correct
---

# Parenthésage correct

Avant d'évaluer une expression, on souhaite vérifier qu'elle est correctement parenthésée.

On appelle **expression correctement parenthésée** une expression dans laquelle, à chaque position, le nombre d'_ouvrants_ d'une catégorie situés avant cette position est toujours supérieur ou égal au nombre de _fermants_ de la même catégorie situés avant cette position.

Par exemple, l'expression `#!py "3*(8+2)"` est correctement parenthésée alors que l'expression `#!py "[{8, 2})"` ne l'est pas.

On considère plusieurs catégories de **délimiteurs** :

* les parenthèses `#!py "("` et `#!py ")"`,

* les crochets `#!py "["` et `#!py "]"`,

* les accolades `#!py "{"` et `#!py "}"`.

Ces délimiteurs sont de deux **genres** :

* certains sont _ouvrants_, comme `#!py "("`, `#!py "["` et `#!py "{"`.

* les autres sont _fermants_, comme `#!py ")"`, `#!py "]"` et `#!py "}"`.

Les différents délimiteurs sont regroupés dans un dictionnaire `{délimiteur: (genre, complémentaire)}` qui associe par exemple à la clé `#!py "]"` le tuple `("fermant", "[")`.

## Objectif

On demande d'écrire la fonction `parenthesage_correct` qui prend en paramètre une chaine de caractères `entree` et renvoyant `True` si elle est correctement parenthésée, `False` dans le cas contraire.

## Exemples

```pycon
>>> parenthesage_correct("3*(8+2)")
True
>>> parenthesage_correct("[3*(8+2)]")
True
>>> parenthesage_correct("[{8, 2})")
False
>>> parenthesage_correct("()()[[[]]]")
True
>>> parenthesage_correct("()()[[[]]")
False
```
