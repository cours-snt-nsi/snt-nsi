---
title: 📁 Tous les exercices
hide:
    -toc
---
!!! success "Site SNT NSI avec Python"

    ![](../assets/LogoLaForge.svg){ width=50% }![](./assets/qrcode_atelier_forge.png){ width=20% align=right}

    Ce site est le document d'accompagnement de la SNT ET NSI (2024-2025).

??? abstract "A"

    - [ABR](../full_exos/A/ABR/index.md)


??? abstract "B"


Voici le code Markdown avec la liste de répertoires demandée :

