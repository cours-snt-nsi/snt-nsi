---
title: Accueil
hide:
    -toc
    -navigation
---
!!! success "Site SNT NSI avec Python"

    ![](./assets/LogoLaForge.svg){ width=50% }![](./assets/qrcode_atelier_forge.png){ width=20% align=right}

    Ce site est le document d'accompagnement de la SNT ET NSI (2024-2025).

!!! abstract "Sommaire"

    -   [Présentation](./presentation.md)
    -   [SNT](./01_SNT/index.md)
    -   [1er NSI](./02_1NSI/index.md)
    -   [Ter NSI](./03_TNSI/index.md)
    -   [Liens Utils](./04_links/index.md)



