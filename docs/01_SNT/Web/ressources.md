---
author: à compléter
title: 📚 Ressources
---

# Le Web

## Introduction au web

!!! abstract "Présentation"

    Dans cette activité, on découvre le thème Web à travers une vidéo tirée du MOOC SNT. 


??? note "01. Introduction au web"

	<div class="centre">
	<iframe 
	src="../a_telecharger/01/01. Introduction au web.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>

[Ressources 01. Introduction au web](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Web/a_telecharger/01){ .md-button target="_blank" rel="noopener" }

## Repères historiques - Web

!!! abstract "Présentation"

    Dans cette activité, les élèves vont découvrir les dates clés en lien avec le web et les situer dans un contexte historique plus large.

	Ils créeront une frise chronologique et des supports de révision. 


??? note "02. Repères historiques Web"

	<div class="centre">
	<iframe 
	src="../a_telecharger/02/02. Repères historiques Web.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 02. Repères historiques Web](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Web/a_telecharger/02){ .md-button target="_blank" rel="noopener" }

##  Images, liens et moteurs de recherche

!!! abstract "Présentation"

    Dans cette activité, on s’intéresse à l’enrichissement des pages web : insertions d’images et liens hypertextes et on présente quelques moteurs de recherche.

	Les élèves observeront et manipuleront des parties de code. 


??? note "03. Images, liens et moteurs de recherche"

	<div class="centre">
	<iframe 
	src="../a_telecharger/03/03. Images liens et moteurs de recherche - WEB.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 03. Images, liens et moteurs de recherche](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Web/a_telecharger/03){ .md-button target="_blank" rel="noopener" }

##  L’algorithme de PageRank

!!! abstract "Présentation"

    Dans cette activité, on présente les principes des moteurs de recherche, en particulier l’algorithme PageRank qui permet de mesurer la popularité d’un site ou d’une page web.


??? note "04. L’algorithme de PageRank"

	<div class="centre">
	<iframe 
	src="../a_telecharger/04/04. Algorithme de PageRank - WEB.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 04. L’algorithme de PageRank](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Web/a_telecharger/04){ .md-button target="_blank" rel="noopener" }

##  URL, protocole HTTP et modèle client-serveur

!!! abstract "Présentation"

    Dans cette activité, on présente un principe de base du web (le modèle client-serveur), la notion d’URL et le protocole HTTP de communication entre le client et le serveur.


??? note "05. URL, protocole HTTP et modèle client-serveur"

	<div class="centre">
	<iframe 
	src="../a_telecharger/05/05. URL protocole HTTP et modèle clientserveur - WEB.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 05. URL, protocole HTTP et modèle client-serveur](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Web/a_telecharger/05){ .md-button target="_blank" rel="noopener" }

##  Principe du modèle client-serveur

!!! abstract "Présentation"

    Dans cette activité, on présente les grands principes du modèle client-serveur. On montre en particulier un exemple de dialogue simple entre un client et un serveur et on aborde la notion de pages dynamiques. 


??? note "06. Principe du modèle client-serveur"

	<div class="centre">
	<iframe 
	src="../a_telecharger/06/06. Principe du modèle client-serveur - WEB.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 06. Principe du modèle client-serveur](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Web/a_telecharger/06){ .md-button target="_blank" rel="noopener" }

##  Navigateurs et langages HTML et CSS

!!! abstract "Présentation"

    Dans cette activité, on présente quelques navigateurs classiques et une introduction aux langages HTML et CSS.

	Les élèves observeront et manipuleront des parties de code.


??? note "07. Navigateurs et langages HTML et CSS"

	<div class="centre">
	<iframe 
	src="../a_telecharger/07/07. Navigateurs et langages HTML CSS - WEB.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 07. Navigateurs et langages HTML et CSS](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Web/a_telecharger/07){ .md-button target="_blank" rel="noopener" }

##  Découverte du langage HTML

!!! abstract "Présentation"

    Dans cette activité, les élèves vont découvrir la structure d’une page Web et comment la modifier. 


??? note "08. Découverte du langage HTML"

	<div class="centre">
	<iframe 
	src="../a_telecharger/08/08. Decouverte HTML - WEB.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 08. Découverte du langage HTML](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Web/a_telecharger/08){ .md-button target="_blank" rel="noopener" }

##  Découverte du langage css

!!! abstract "Présentation"

    Dans cette activité, on présente les bases du langage CSS et on montre comment une feuille de style permet de modifier l’apparence d’une page web. Les élèves manipulent et produisent du code, et revoient les notions de client-serveur.


??? note "09. Découverte du langage css"

	<div class="centre">
	<iframe 
	src="../a_telecharger/09/09. Decouverte CSS - WEB.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 09. Découverte du langage css](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Web/a_telecharger/09){ .md-button target="_blank" rel="noopener" }

##  Les Cookies

!!! abstract "Présentation"

    Dans cette activité, on présente la notion de cookies, et on apprend à paramétrer son navigateur web.

??? note "10. Les Cookies"

	<div class="centre">
	<iframe 
	src="../a_telecharger/10/10. Les Cookies - WEB.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 10. Les Cookies](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Web/a_telecharger/10){ .md-button target="_blank" rel="noopener" }

