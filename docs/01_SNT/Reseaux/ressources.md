---
author: à compléter
title: 📚 Ressources
---

# Les réseaux sociaux

## Introduction aux réseaux sociaux

!!! abstract "Présentation"

    Dans cette activité, les élèves découvrent le thème des Réseaux sociaux à travers une vidéo tirée du Mooc SNT, une rédaction de texte autour de questions ainsi que des échanges sur des thématiques générales liées aux réseaux sociaux.



??? note "01. Introduction aux réseaux sociaux"

	<div class="centre">
	<iframe 
	src="../a_telecharger/01/01 - Introduction aux réseaux sociaux - RESEAUX SOCIAUX.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 01. Introduction aux réseaux sociaux](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Reseaux/a_telecharger/01){ .md-button target="_blank" rel="noopener" }


## Exposés : les réseaux sociaux

!!! abstract "Présentation"

    Dans cette activité, on propose aux élèves de préparer des exposés concernant les réseaux sociaux comme Twitter, Snapchat ou TikTok par exemple.



??? note "02. Exposés : les réseaux sociaux"

	<div class="centre">
	<iframe 
	src="../a_telecharger/02/02 - Exposés - RESEAUX SOCIAUX.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 02. Introduction aux réseaux sociaux](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Reseaux/a_telecharger/02){ .md-button target="_blank" rel="noopener" }

## Réseaux sociaux et graphes

!!! abstract "Présentation"

    Dans cette activité, les élèves étudient les relations amicales dans deux exemples de réseaux sociaux. Ils apprennent 
	à modéliser un réseau social sous la forme d’un tableau ou d’un graphe afin d’en extraire des informations pertinentes.



??? note "03. Réseaux sociaux et graphes"

	<div class="centre">
	<iframe 
	src="../a_telecharger/03/03 - Les graphes - RESEAUX SOCIAUX .pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 03. Réseaux sociaux et graphes](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Reseaux/a_telecharger/03){ .md-button target="_blank" rel="noopener" }


## Le petit monde de Milgram

!!! abstract "Présentation"

    Dans cette activité, les élèves découvrent la notion de “petit monde” grâce à l’expérience de Milgram, puis le lien est 
	fait avec les réseaux sociaux.


??? note "04 - Le petit monde de Milgram"

	<div class="centre">
	<iframe 
	src="../a_telecharger/04/04 - Le petit monde de Milgram - RESEAUX SOCIAUX.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 04 - Le petit monde de Milgram](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Reseaux/a_telecharger/04){ .md-button target="_blank" rel="noopener" }


## Modèles économiques

!!! abstract "Présentation"

    Dans cette activité, les élèves découvrent comment les réseaux sociaux génèrent leurs principales sources de revenus et en interrogent l’aspect éthique.


??? note "05 - Modèles économiques"

	<div class="centre">
	<iframe 
	src="../a_telecharger/05/05 - Modèles économiques - RESEAUX SOCIAUX.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>



[Ressources 05 - Modèles économiques](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Reseaux/a_telecharger/05){ .md-button target="_blank" rel="noopener" }


##  Modèles économiques : le rachat d'Instagram

!!! abstract "Présentation"

    Dans cette activité, il s’agit de comprendre le modèle économique des réseaux sociaux par le rachat d’autres réseaux sociaux et les raisons.




??? note "06 - Le rachat d'Instagram"

	<div class="centre">
	<iframe 
	src="../a_telecharger/06/06 - Modèle économique - rachat d'Instagram par Facebook - RESEAUX SOCIAUX.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 06 - Le rachat d'Instagram](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Reseaux/a_telecharger/06){ .md-button target="_blank" rel="noopener" }



## Recherche de l’authenticité perdue

!!! abstract "Présentation"

    Dans cette activité, il s’agit de s’interroger, à travers l’étude de l’application BeReal, sur la nouvelle tendance de recherche d’authenticité sur les réseaux. Une recherche vouée à l’échec ?


??? note "07 - Recherche d'authenticité"

	<div class="centre">
	<iframe 
	src="../a_telecharger/07/07 - Recherche d'authenticité - RESEAUX SOCIAUX.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 07 - Recherche d'authenticité](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Reseaux/a_telecharger/07){ .md-button target="_blank" rel="noopener" }


## E-réputation et identité numérique

!!! abstract "Présentation"

	Dans cette activité, les élèves découvrent la notion d’identité numérique, ou e-réputation, à l’aide de documents et vidéos puis apprennent de bonnes pratiques pour protéger leur identité numérique.



??? note "08 - e-réputation et identité numérique"

	<div class="centre">
	<iframe 
	src="../a_telecharger/08/08 - E-réputation et identité numérique - RESEAUX SOCIAUX.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 08 - e-réputation et identité numérique](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Reseaux/a_telecharger/08){ .md-button target="_blank" rel="noopener" }


## Cyberviolence


!!! abstract "Présentation"

    Dans cette activité, les élèves vont contextualiser la thématique en visionnant des vidéos et en répondant à des questions. En particulier, chercher les différentes formes de cyberviolence (harcèlement, discrimination, sexting, …) et les ressources disponibles pour lutter contre celles-ci.


??? note "09 - Cyberviolence"

	<div class="centre">
	<iframe 
	src="../a_telecharger/09/09 - Cyberviolence - RESEAUX SOCIAUX.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 09 - Cyberviolence](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Reseaux/a_telecharger/09){ .md-button target="_blank" rel="noopener" }


## Activité pratique : J’affiche mon message contre la cyberviolence


!!! abstract "Présentation"

    Dans cette activité, les élèves vont réaliser des affiches sur la cyberviolence qui pourront faire l’objet d’une exposition dans l’établissement ou dans la classe.



??? note "10 - Activité pratique : J’affiche mon message contre la cyberviolence"

	<div class="centre">
	<iframe 
	src="../a_telecharger/10/10 - J'affiche mon message contre la cyberviolence - RESEAUX SOCIAUX.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


[Ressources 10 - Activité pratique : J’affiche mon message contre la cyberviolence](https://forge.apps.education.fr/ressources/snt/-/tree/main/docs/Reseaux/a_telecharger/10){ .md-button target="_blank" rel="noopener" }
