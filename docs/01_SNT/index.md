---
title: 🙋 Enseigner les SNT
hide:
    -toc
---


# Enseigner les SNT par l'association AEIF ![](images/logoSNT.png){: .center width="130"}

!!! abstract "Présentation"


    L’enseignement de sciences numériques et technologie en classe de seconde a pour objet de permettre d’appréhender les principaux concepts des sciences numériques, mais également de permettre aux élèves, à partir d’un objet technologique, de comprendre le poids croissant du numérique et les enjeux qui en découlent. La numérisation généralisée des données, les nouvelles modalités de traitement ou de stockage et le développement récent d’algorithmes permettant de traiter de très grands volumes de données numériques constituent une réelle rupture dans la diffusion des technologies de l’information et de la communication. Cette révolution multiplie les impacts majeurs sur les pratiques humaines.

!!! abstract "Programme de la matière SNT"

    Le programme de SNT aborde les thèmes suivants :

    * [Internet et le Web](./Internet/presentation.md)
    * [Les réseaux sociaux](./Reseaux/presentation.md)
    * [Les données structurées et leur traitement](./Donnees/presentation.md)
    * [Localisation, cartographie et mobilité](./Localisation/presentation.md)
    * [Informatique embarquée et objets connectés](./Objets/presentation.md)
    * [La photographie numérique](./Photo/presentation.md)

??? note "Programme de sciences numériques et technologie de seconde générale et technologique"

	<div class="centre">
	<iframe 
	src="./documents/BO.pdf"
	width="1000" height="1000" 
	frameborder="0" 
	allow="autoplay; fullscreen; picture-in-picture" allowfullscreen>
	</iframe>
	</div>


!!! abstract "Objectifs de la matière SNT"

    La matière SNT a pour principaux objectifs de permettre aux élèves de :

    * Comprendre les concepts fondamentaux du numérique et de l'informatique
    * Développer un esprit critique vis-à-vis des technologies numériques
    * Apprendre à utiliser et à produire des contenus et des services numériques
    * Sensibiliser aux enjeux éthiques, sociaux et environnementaux du numérique


!!! tip "Environ la moitié du temps de la matière est consacrée à des travaux pratiques sur ordinateur ou du matériel de type informatique embarqué."

!!! question "Exemples de poursuites d'études en première"

    Les élèves ayant suivi l'enseignement de SNT en seconde pourront par exemple poursuivre en première :

    * Bac Général avec la spécialité Numérique et Sciences Informatiques (NSI)
    * Bac Technologique avec la série Sciences et Technologies de l'Industrie et du Développement Durable (STI2D) 
    * Bac Professionnel dans les domaines de l'informatique, du numérique et des technologies
    * BTS dans les secteurs de l'informatique, du multimédia, des télécommunications

!!! success "Cette matière offre ainsi aux élèves une solide introduction aux enjeux du numérique, tout en leur ouvrant de nombreuses perspectives de poursuite d'études dans ces secteurs en forte croissance."
