

# --------- PYODIDE:code --------- #

class Noeud:
    def __init__(self, gauche, element, droite):
        self.gauche = gauche
        self.element = element
        self.droite = droite

class ABR:
    "Classe ABR ; sans doublon"

    def __init__(self):
        "Initialiseur"
        self.racine = None

    def est_vide(self):
        "Renvoie un booléen"
        return self.racine is None

    def insere(self, element):
        "Insère sans doublon un élément"
        if self.est_vide():
            self.racine = Noeud(ABR(), element, ABR())
        elif element < self.racine.element:
            self.racine.gauche.insere(element)
        elif element > self.racine.element:
            ...
        else:
            # cas d'égalité
            pass  # donc pas de doublon !

    def est_present(self, element):
        "Renvoie un booléen"
        if self.est_vide():
            return ...
        elif element < self.racine.element:
            return self.racine.gauche.est_present(element)
        elif ...:
            ...
        else:
            # cas d'égalité
            return True

    def affichage_infixe(self):
        "Renvoie une chaine à afficher"
        if self.est_vide():
            return "|"
        else:
            return (
                  self.racine.gauche.affichage_infixe()
                + str(self.racine. ...)
                + self.racine. ...
            )

# --------- PYODIDE:corr --------- #

class Noeud:
    def __init__(self, gauche, element, droite):
        self.gauche = gauche
        self.element = element
        self.droite = droite


class ABR:
    "Classe ABR ; sans doublon"

    def __init__(self):
        "Initialiseur"
        self.racine = None

    def est_vide(self):
        "Renvoie un booléen"
        return self.racine is None

    def insere(self, element):
        "Insère sans doublon un élément"
        if self.est_vide():
            self.racine = Noeud(ABR(), element, ABR())
        elif element < self.racine.element:
            self.racine.gauche.insere(element)
        elif element > self.racine.element:
            self.racine.droite.insere(element)
        else:
            # cas d'égalité
            pass  # donc pas de doublon !

    def est_present(self, element):
        "Renvoie un booléen"
        if self.est_vide():
            return False
        elif element < self.racine.element:
            return self.racine.gauche.est_present(element)
        elif element > self.racine.element:
            return self.racine.droite.est_present(element)
        else:
            # cas d'égalité
            return True

    def affichage_infixe(self):
        "Renvoie une chaine à afficher"
        if self.est_vide():
            return "|"
        else:
            return (
                self.racine.gauche.affichage_infixe()
                + str(self.racine.element)
                + self.racine.droite.affichage_infixe()
            )

# --------- PYODIDE:tests --------- #

#1
nombres = ABR()
assert nombres.est_vide()
for x in [1, 3, 7, 9, 9]:
    nombres.insere(x)
assert nombres.est_vide() is False

assert nombres.affichage_infixe() == '|1|3|7|9|'

assert nombres.est_present(7)
assert nombres.est_present(8) is False

#2
fruits_ranges = ABR()
assert fruits_ranges.est_vide()

panier = ["kiwi", "pomme", "abricot", "mangue", "poire"]
for fruit in panier:
    fruits_ranges.insere(fruit)
assert fruits_ranges.est_vide() is False

assert fruits_ranges.affichage_infixe() == '|abricot|kiwi|mangue|poire|pomme|'

assert fruits_ranges.est_present("pomme")
assert fruits_ranges.est_present("cerise") is False

# --------- PYODIDE:secrets --------- #

# tests

# 1
nombres = ABR()
assert nombres.est_vide()
for x in [1, 3, 7, 9, 9]:
    nombres.insere(x)
assert nombres.est_vide() is False

assert nombres.affichage_infixe() == "|1|3|7|9|"

assert nombres.est_present(7)
assert nombres.est_present(8) is False

# 2
fruits_ranges = ABR()
assert fruits_ranges.est_vide()

panier = ["kiwi", "pomme", "abricot", "mangue", "poire"]
for fruit in panier:
    fruits_ranges.insere(fruit)
assert fruits_ranges.est_vide() is False

assert fruits_ranges.affichage_infixe() == "|abricot|kiwi|mangue|poire|pomme|"

assert fruits_ranges.est_present("pomme")
assert fruits_ranges.est_present("cerise") is False


# autres tests


def permutations(objets):
    n = len(objets)
    if n == 0:
        return [[]]
    resultat = []
    for i in range(n):
        objets_sans_i = [objets[j] for j in range(n) if i != j]
        for perm in permutations(objets_sans_i):
            perm.append(objets[i])
            resultat.append(perm)
    return resultat


for perm in permutations([1, 2, 3, 4, 5]):
    abr = ABR()
    assert abr.est_vide()
    for x in perm:
        abr.insere(x)
    for x in range(-5, 10):
        attendu = 1 <= x < 6
        assert abr.est_present(x) == attendu
    assert abr.affichage_infixe() == "|1|2|3|4|5|"
