---
author: Franck Chambon
hide:
    - navigation
    - toc
title: Arbre binaire de recherche
tags:
    - à trous
    - arbre binaire
    - récursivité
    - programmation orientée objet
    - ep2
difficulty: 301
---
# Arbre binaire de recherche

On donne une partie de la classe `ABR` pour implémenter les **A**rbres **B**inaires de **R**echerche **sans doublon** : un ensemble fini de nœuds, éventuellement vide, organisés de manière hiérarchique. C'est une arborescence d'éléments comparables.

Un ABR est une structure de nature récursive :

- soit c'est un ABR vide,
- soit il possède un nœud `racine` qui a les attributs :
    - `gauche` : un sous-ABR à gauche
    - `element` : un élément comparable aux autres
    - `droite` : un sous-ABR à droite
    - l'élément de la racine est strictement supérieur à celui du sous-ABR gauche (s'il est non vide)
    - l'élément de la racine est strictement inférieur à celui du sous-ABR droite (s'il est non vide)

Dans l'implémentation suivante :

- `Noeud` est une classe qui possède trois attributs :
    - `gauche`
    - `element`
    - `droite`
    - On aurait pu utiliser aussi un tuple nommé...

- `#!py ABR()` initialise un ABR vide.
- Un ABR, **vide ou non**, possède les méthodes dont le nom est explicite :
    - `est_vide(self)` qui renvoie un booléen
    - `insere(self, element)` qui agit sur l'ABR
    - `est_present(self, element)` qui renvoie un booléen
    - `affichage_infixe(self)` qui renvoie une chaine de caractère composée des affichages des éléments et du séparateur `|`, suite à un parcours infixe.

???+ example "Exemples"

    ```pycon title=""
    >>> nombres = ABR()
    >>> nombres.est_vide()
    True
    >>> for x in [1, 3, 7, 9, 9]: nombres.insere(x)
    >>> not nombres.est_vide()
    True
    >>> nombres.affichage_infixe()
    '|1|3|7|9|'
    >>> nombres.est_present(7)
    True
    >>> nombres.est_present(8)
    False
    >>> fruits_ranges = ABR()
    >>> fruits_ranges.est_vide()
    True
    >>> panier = ["kiwi", "pomme", "abricot", "mangue", "poire"]
    >>> for fruit in panier: fruits_ranges.insere(fruit)
    >>> fruits_ranges.est_vide()
    False
    >>> fruits_ranges.affichage_infixe()
    '|abricot|kiwi|mangue|poire|pomme|'
    >>> fruits_ranges.est_present("pomme")
    True
    >>> fruits_ranges.est_present("cerise")
    False
    ```

Compléter les méthodes ci-dessous.

{{ IDE('exo') }}
